#ifndef _QSPI_H_
#define _QSPI_H_

#include <stdint.h>

void qspi_init();
void qspi_cs(int cs, int active);
void qspi_transfer_half(void *user, int cs, const void *out, int out_len, void *in, int in_len);

#endif
