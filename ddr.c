#include "ddr.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

ddr_t *ddr_open(char *path, char *search)
{
	ddr_t *d;
	FILE *f;
	char line[512], *p;
	uint32_t reg, val;
	int i, num;

	if (path == NULL)
		return NULL;

	d = malloc(sizeof(ddr_t));
	if (d == NULL)
		return NULL;

	f = fopen(path, "rb");
	if (f == NULL)  {
		if (search != NULL) {
			i = snprintf(line, 511, "%s/sys_cfg/%s", search, path);
			if(i > 511)
				i = 511;
			line[i] = 0;
			f = fopen(line, "rb");
		}
	}
	if (f == NULL) {
		ddr_close(d);
		return NULL;
	}
	num = 0;
	d->count = 0;
	d->data = NULL;
	d->xusb_path = NULL;
	while (!feof(f)) {
		if (fgets(line, 512, f) == NULL)
			break;
		line[511] = 0;
		p = strchr(line, '=');
		if (p == NULL)
			continue;
		p[0] = 0;
		reg = strtoul(line, NULL, 0);
		if (reg == 0xb0000000)
			continue;
		val = strtoul(&p[1], NULL, 0);

		if (d->count >= num) {
			num = num + 32;
			p = realloc(d->data, 8 + num + 32);
			if (p == NULL) {
				fclose(f);
				ddr_close(d);
				return NULL;
			}
			d->data = p;
		}

		((uint8_t *)d->data)[8 + d->count + 0] = ((reg >> 0) & 0xFF);
		((uint8_t *)d->data)[8 + d->count + 1] = ((reg >> 8) & 0xFF);
		((uint8_t *)d->data)[8 + d->count + 2] = ((reg >> 16) & 0xFF);
		((uint8_t *)d->data)[8 + d->count + 3] = ((reg >> 24) & 0xFF);
		d->count += 4;

		((uint8_t *)d->data)[8 + d->count + 0] = ((val >> 0) & 0xFF);
		((uint8_t *)d->data)[8 + d->count + 1] = ((val >> 8) & 0xFF);
		((uint8_t *)d->data)[8 + d->count + 2] = ((val >> 16) & 0xFF);
		((uint8_t *)d->data)[8 + d->count + 3] = ((val >> 24) & 0xFF);
		d->count += 4;

		if (reg == 0xb0002010) {
			if ((val & 0x07) == 4) {
				d->xusb_path = "xusb16.bin";
			} else if((val & 0x07) == 5) {
				d->xusb_path = "xusb.bin";
			} else if((val & 0x07) == 6) {
				d->xusb_path = "xusb64.bin";
			} else if((val & 0x07) == 7) {
				d->xusb_path = "xusb128.bin";
			}
		}
		if (d->count >= 512)
			break;
	}
	fclose(f);
	if ((d->xusb_path == NULL) || (d->count < 8)) {
		ddr_close(d);
		return NULL;
	}
	d->buf = &((uint8_t *)d->data)[8];
	d->data_count = ((d->count + 8 + 15) / 16) * 16;
	((uint8_t *)d->data)[0] = 0x55;
	((uint8_t *)d->data)[1] = 0xAA;
	((uint8_t *)d->data)[2] = 0x55;
	((uint8_t *)d->data)[3] = 0xAA;
	((uint8_t *)d->data)[4] = (((d->count / 8) >> 0) & 0xFF);
	((uint8_t *)d->data)[5] = (((d->count / 8) >> 8) & 0xFF);
	((uint8_t *)d->data)[6] = (((d->count / 8) >> 16) & 0xFF);
	((uint8_t *)d->data)[7] = (((d->count / 8) >> 24) & 0xFF);
	for (i = (8 + d->count); i < d->data_count; i++) {
		((uint8_t *)d->data)[i] = 0x00;
	}
	return d;
}

void ddr_close(ddr_t *d)
{
	free(d->data);
	free(d);
}

