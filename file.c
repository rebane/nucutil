#include "file.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

static void file_store32(void *dst, int offset, uint32_t val);

file_t *file_open(char *path, int offset, int count)
{
	struct stat s;
	file_t *f;
	int fd;

	if (path == NULL)
		return NULL;

	f = malloc(sizeof(file_t));
	if (f == NULL)
		return NULL;

	f->count = count;
	fd = open(path, O_RDONLY);
	if (fd < 0) {
		free(f);
		return NULL;
	}
	if (f->count < 0) {
		if ((fstat(fd, &s) < 0) || (offset > s.st_size)) {
			close(fd);
			free(f);
			return NULL;
		}
		f->count = s.st_size - offset;
	}
	f->buf = malloc(f->count);
	if (f->buf == NULL) {
		close(fd);
		free(f);
		return NULL;
	}
	if (lseek(fd, offset, SEEK_SET) < 0) {
		close(fd);
		file_close(f);
		return NULL;
	}
	if (read(fd, f->buf, f->count) != f->count) {
		close(fd);
		file_close(f);
		return NULL;
	}
	close(fd);
	return f;
}

// 20 54 56 4E  00 80 00 00  08 80 00 00  FF FF FF FF  00 08 40 00  EB 35 31 02  03 FF FF FF  FF FF FF FF  55 AA 55 AA  2E 00 00 00  64 02 00 B0

int file_write_boot(file_t *f, char *path, uint32_t exec_addr, int type, void *ddr_buf, int ddr_count)
{
	uint8_t header[32];
	int fd;

	fd = open(path, O_WRONLY | O_CREAT, 0644);
	if (fd < 0)
		return -1;

	file_store32(header, 0, 0x4e565420);  // boot code marker
	file_store32(header, 4, exec_addr);   // execution address
	file_store32(header, 8, f->count);    // image size
	file_store32(header, 12, 0xffffffff);
	file_store32(header, 16, 0x00400800); // (info.SPINand_SpareArea << 16)|(info.SPINand_PageSize)
	if (type == FILE_TYPE_SPINOR)
		file_store32(header, 20, 0x023135EB); // StatusValue << 24, WriteStatusCmd << 16, ReadStatusCmd << 8, QuadReadCmd
	else if (type == FILE_TYPE_SPINAND)
		file_store32(header, 20, 0xFFFFFF6B);
	else
		file_store32(header, 20, 0xFFFFFF6B);

	if (type == FILE_TYPE_SPINOR)
		file_store32(header, 24, 0xFFFFFF03);
	else if (type == FILE_TYPE_SPINAND)
		file_store32(header, 24, 0xFFFFFF01);
	else if (type == FILE_TYPE_SD)
		file_store32(header, 24, 0xFFFFFF00);
	else
		file_store32(header, 24, 0xFFFFFF00);

	file_store32(header, 28, 0xFFFFFFFF);
	if (write(fd, header, 32) != 32) {
		close(fd);
		return -1;
	}
	if (write(fd, ddr_buf, ddr_count) != ddr_count) {
		close(fd);
		return -1;
	}
	if (write(fd, f->buf, f->count) != f->count) {
		close(fd);
		return -1;
	}
	close(fd);
	return 0;
}

void file_close(file_t *f)
{
	free(f->buf);
	free(f);
}

static void file_store32(void *dst, int offset, uint32_t val)
{
	((uint8_t *)dst)[offset + 0] = (val >> 0) & 0xFF;
	((uint8_t *)dst)[offset + 1] = (val >> 8) & 0xFF;
	((uint8_t *)dst)[offset + 2] = (val >> 16) & 0xFF;
	((uint8_t *)dst)[offset + 3] = (val >> 24) & 0xFF;
}

