#ifndef _USB_H_
#define _USB_H_

#include <stdint.h>

#define USB_PORTS_MAX 64

struct usb_path_struct{
	int count;
	uint8_t port[USB_PORTS_MAX];
};

typedef struct usb_path_struct usb_path_t;

struct usb_device_list_struct{
	int count;
	usb_path_t *path;
};

typedef struct usb_device_list_struct usb_device_list_t;

typedef struct usb_device_handle_struct usb_device_handle_t;

int usb_init();
usb_device_list_t *usb_get_device_list(uint16_t vid, uint16_t pid);
void usb_free_device_list(usb_device_list_t *ul);
usb_path_t *usb_parse_path(char *pathstring);
void usb_free_path(usb_path_t *path);
usb_device_handle_t *usb_open(uint16_t vid, uint16_t pid, usb_path_t *path);
int usb_claim_interface(usb_device_handle_t *dh, int interface);
int usb_release_interface(usb_device_handle_t *dh, int interface);
int usb_reset_device(usb_device_handle_t *dh);
int usb_control_transfer(usb_device_handle_t *dh, uint8_t bm_requestType, uint8_t b_request, uint16_t w_value, uint16_t w_index, void *data, uint16_t w_length, unsigned int timeout);
int usb_bulk_transfer(usb_device_handle_t *dh, uint8_t endpoint, void *data, int length, int *transferred, unsigned int timeout);
void usb_close(usb_device_handle_t *dh);

#endif

