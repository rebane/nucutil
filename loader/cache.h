#ifndef _CACHE_H_
#define _CACHE_H_

void cache_flush_and_clean_d();
void cache_disable();
void cache_flush_i();
void cache_flush_d();
void cache_flush_id();
void cache_setup_cp15(unsigned int addr);
void cache_enable_write_back();

#endif

