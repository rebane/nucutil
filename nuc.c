#include "nuc.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "usb.h"

#define NUC_USB_TIMEOUT      10000
#define NUC_USB_ENDPOINT_IN  0x81
#define NUC_USB_ENDPOINT_OUT 0x02

struct nuc_struct{
	usb_path_t path;
	usb_device_handle_t *dh;
	int type;
	int fw_loaded;
};

static void nuc_store32(void *dst, int offset, uint32_t val);
static int nuc_open_usb(nuc_t *n);
static int nuc_ack(nuc_t *n, uint32_t *ack);
static int nuc_set_type(nuc_t *n, int type);
static int nuc_read_pipe(nuc_t *n, void *buf, int count);
static int nuc_write_pipe(nuc_t *n, const void *buf, int count);
static int nuc_write_buffer(nuc_t *n, const void *buf, int count, void *user, void(*progress)(void *user, int written, int count));
static int nuc_close_usb(nuc_t *n);

nuc_t *nuc_open(usb_path_t *path)
{
	nuc_t *n;
	n = malloc(sizeof(nuc_t));
	if (n == NULL)
		return NULL;
	memcpy(&n->path, path, sizeof(usb_path_t));
	if (nuc_open_usb(n) < 0) {
		free(n);
		return NULL;
	}
	n->type = -1;
	n->fw_loaded = 0;
	return n;
}

int nuc_ddr(nuc_t *n, const void *buf, int count)
{
	uint8_t header[8];
	uint32_t ack;
	nuc_store32(header, 0, count);
	nuc_store32(header, 4, 16);
	if (nuc_write_pipe(n, header, 8) < 0)
		return -1;

	if (nuc_write_pipe(n, buf, count) < 0)
		return -1;

	if (nuc_ack(n, &ack) < 0)
		return -1;

	if (ack == (4096 + 1)) {
		n->fw_loaded = 1;
		return 0;
	}

	if (nuc_ack(n, &ack) < 0)
		return -1;

	if (ack != 0x55AA55AA)
		return -1;

	n->fw_loaded = 0;
	return 0;
}

int nuc_fw(nuc_t *n, uint32_t address, const void *buf, int count)
{
	uint8_t header[8];
	int try;

	if (n->fw_loaded)
		return 0;

	nuc_store32(header, 0, count);
	nuc_store32(header, 4, address);
	if (nuc_write_pipe(n, header, 8) < 0)
		return -1;

	if (nuc_write_buffer(n, buf, count, NULL, NULL) < 0)
		return -1;

	nuc_close_usb(n);
	for (try = 0; try < 32; try++) {
		if (nuc_open_usb(n) >= 0)
			return 0;
		usleep(250000);
	}
	return -1;
}

int nuc_info(nuc_t *n, nuc_info_t *info)
{
	uint8_t buf[80];
	uint32_t ack;

	memset(buf, 0, 80);
	if (nuc_set_type(n, NUC_TYPE_INFO) < 0)
		return -1;

	if (nuc_write_pipe(n, buf, 80) < 0)
		return -1;

	if (nuc_read_pipe(n, buf, 80) < 0)
		return -1;

	if (nuc_ack(n, &ack) < 0)
		return -1;

	if ((ack & 0xFF) != 0x90)
		return -1;

	return 0;
}

int nuc_write_dram(nuc_t *n, int run, uint32_t address, const void *buf, int count, void *user, void(*progress)(void *user, int written, int count))
{
	uint8_t header[16];
	uint32_t ack;

	if (progress)
		progress(user, 0, count);

	if (nuc_set_type(n, NUC_TYPE_SDRAM) < 0)
		return -1;

	if (run)
		address |= 0x80000000;

	nuc_store32(header, 0, 0);        // write action
	nuc_store32(header, 4, count);    //
	nuc_store32(header, 8, address);  //
	nuc_store32(header, 12, 0);       //
	if (nuc_write_pipe(n, header, 16) < 0)
		return -1;

	if (nuc_ack(n, &ack) < 0)
		return -1;

	if (nuc_write_buffer(n, buf, count, user, progress) < 0)
		return -1;

	if (progress)
		progress(user, count, count);

	return 0;
}

int nuc_write_spi_nand(nuc_t *n, uint32_t address, const void *buf, int count, void *user, void(*progress)(void *user, int written, int count))
{
	uint8_t header[56];
	uint32_t ack;

	if (progress)
		progress(user, 0, count);

	if (nuc_set_type(n, NUC_TYPE_SPINAND) < 0)
		return -1;

	nuc_store32(header, 0, 0);        // write action
	nuc_store32(header, 4, count);    // len
	nuc_store32(header, 8, 0);        // no
	memset(&header[12], 0, 16);       // name
	nuc_store32(header, 28, 0);       // type
	nuc_store32(header, 32, 0x200);   // exec_addr
	nuc_store32(header, 36, address); // flash_offset
	nuc_store32(header, 40, 0);       // end_addr
	memset(&header[44], 0, 8);        // mac_addr
	nuc_store32(header, 52, 0);       // init_size
	if (nuc_write_pipe(n, header, 56) < 0)
		return -1;

	if (nuc_ack(n, &ack) < 0)
		return -1;

	if (nuc_write_buffer(n, buf, count, user, progress) < 0)
		return -1;

	if (progress)
		progress(user, count, count);

	return(0);
}

int nuc_write_spi_nand_sync(nuc_t *n, void *user, void(*progress)(void *user, int written, int count))
{
	uint32_t ack;

	if (progress)
		progress(user, 0, 100);

	if (nuc_ack(n, &ack) < 0)
		return -1;

	if (progress)
		progress(user, 100, 100);

	return 0;
}

int nuc_write_spi_nor(nuc_t *n, uint32_t address, const void *buf, int count, void *user, void(*progress)(void *user, int written, int count))
{
	uint8_t header[56];
	uint32_t ack;

	if (progress)
		progress(user, 0, count);

	if (nuc_set_type(n, NUC_TYPE_SPINOR) < 0)
		return -1;

	nuc_store32(header, 0, 0);        // write action
	nuc_store32(header, 4, count);    // len
	nuc_store32(header, 8, 0);        // no
	memset(&header[12], 0, 16);       // name
	nuc_store32(header, 28, 0);       // type
	nuc_store32(header, 32, 0x200);   // exec_addr
	nuc_store32(header, 36, address); // flash_offset
	nuc_store32(header, 40, 0);       // end_addr
	memset(&header[44], 0, 8);        // mac_addr
	nuc_store32(header, 52, 0);       // init_size
	if (nuc_write_pipe(n, header, 56) < 0)
		return -1;

	if (nuc_ack(n, &ack) < 0)
		return -1;

	if (nuc_write_buffer(n, buf, count, user, progress) < 0)
		return -1;

	if (progress)
		progress(user, count, count);

	return 0;
}

int nuc_write_spi_nor_sync(nuc_t *n, void *user, void(*progress)(void *user, int written, int count))
{
	uint32_t ack, pos;

	if (progress)
		progress(user, 0, 100);

	pos = 0;
	while (pos != 100) {
		if (nuc_ack(n, &ack) < 0)
			return -1;

		if(((ack >> 16) & 0xFFFF))
			return -1;

		pos = ack & 0xFF;
		if (progress)
			progress(user, pos, 100);
	}
	if (progress)
		progress(user, 100, 100);

	return(0);
}

void nuc_close(nuc_t *n)
{
	nuc_close_usb(n);
	free(n);
}

static void nuc_store32(void *dst, int offset, uint32_t val)
{
	((uint8_t *)dst)[offset + 0] = (val >> 0) & 0xFF;
	((uint8_t *)dst)[offset + 1] = (val >> 8) & 0xFF;
	((uint8_t *)dst)[offset + 2] = (val >> 16) & 0xFF;
	((uint8_t *)dst)[offset + 3] = (val >> 24) & 0xFF;
}

static int nuc_open_usb(nuc_t *n)
{
	n->dh = usb_open(NUC_VENDOR_ID, NUC_PRODUCT_ID, &n->path);
	if (n->dh == NULL)
		return -1;

	if (usb_claim_interface(n->dh, 0) < 0) {
		usb_close(n->dh);
		return -1;
	}
	if (usb_reset_device(n->dh) < 0) {
		usb_release_interface(n->dh, 0);
		usb_close(n->dh);
		return -1;
	}
	return 0;
}

static int nuc_ack(nuc_t *n, uint32_t *ack)
{
	uint8_t buf[4];

	if (nuc_read_pipe(n, buf, 4) < 0)
		return -1;

	*ack = ((uint32_t)buf[0] << 0) | ((uint32_t)buf[1] << 8) | ((uint32_t)buf[2] << 16) | ((uint32_t)buf[3] << 24);
	return 0;
}

static int nuc_set_type(nuc_t *n, int type)
{
	uint8_t ack[4];
	int i;

	if (n->type == type)
		return 0;

	for (i = 0; i < 16; i++) {
		if (usb_control_transfer(n->dh, 0x40, 0xb0, 0x80 + type, 0, NULL, 0, NUC_USB_TIMEOUT) < 0)
			return -1;

		if (usb_control_transfer(n->dh, 0xC0, 0xb0, 0x80 + type, 0, ack, 4, NUC_USB_TIMEOUT) < 0)
			return -1;

		if (ack[0] == (0x80 + type)) {
			n->type = type;
			return 0;
		}
	}
	return -1;
}

static int nuc_read_pipe(nuc_t *n, void *buf, int count)
{
	int nread;

	if (usb_bulk_transfer(n->dh, NUC_USB_ENDPOINT_IN, buf, count, &nread, NUC_USB_TIMEOUT) < 0)
		return -1;

	if (nread != count)
		return -1;

	return 0;
}

static int nuc_write_pipe(nuc_t *n, const void *buf, int count)
{
	int nwrite;

	if (usb_control_transfer(n->dh, 0x40, 0xa0, 0x12, count, (void *)buf, 0, NUC_USB_TIMEOUT) < 0)
		return -1;

	if (usb_bulk_transfer(n->dh, NUC_USB_ENDPOINT_OUT, (void *)buf, count, &nwrite, NUC_USB_TIMEOUT) < 0)
		return -1;

	if (nwrite != count)
		return -1;

	return 0;
}

static int nuc_write_buffer(nuc_t *n, const void *buf, int count, void *user, void(*progress)(void *user, int written, int count))
{
	uint32_t ack;
	int i, l;

	for (i = 0; i < count; i += 4096) {
		l = count - i;
		if (l > 4096)
			l = 4096;

		if (nuc_write_pipe(n, &((uint8_t *)buf)[i], l) < 0)
			return -1;

		if (nuc_ack(n, &ack) < 0)
			return -1;

		if (ack != l)
			return -1;

		if (progress != NULL)
			progress(user, i, count);
	}
	return 0;
}

static int nuc_close_usb(nuc_t *n)
{
	usb_release_interface(n->dh, 0);
	usb_close(n->dh);
	return 0;
}

