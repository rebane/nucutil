#ifndef _FW_H_
#define _FW_H_

#include <stdint.h>

struct fw_struct{
	uint32_t count;
	uint32_t address;
	uint32_t version;
	void *buf;
	void *data;
};

typedef struct fw_struct fw_t;

fw_t *fw_open(char *path, char *search);
void fw_close(fw_t *fw);

#endif

