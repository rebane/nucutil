#include <stdint.h>
#include "nuc980.h"

static void *memmove(void *dest, const void *src, uint32_t n){
	register char *_dest;
	register const char *_src;
	_dest = dest;
	_src = src;
	if(_src < _dest){ /* Moving from low mem to hi mem; start at end.  */
		for(_src += n, _dest += n; n; n--){
			*--_dest = *--_src;
		}
	}else if(_src != _dest){ /* Moving from hi mem to low mem; start at beginning.  */
		for( ; n; n--){
			*_dest++ = *_src++;
		}
	}
	return(dest);
}

int __attribute__((noinline)) main(){
	volatile int i;
	uint32_t load_addr = LOAD_ADDR;
	static void (*exec_addr)();
	uint32_t count, offset = 416;

	exec_addr = (void *)(*(uint32_t *)(load_addr + 4));
	count = *(uint32_t *)(load_addr + 8);

	if (*(uint32_t *)(load_addr + 0x24) == 0x26)
		offset = 0x160;
	else if (*(uint32_t *)(load_addr + 0x24) == 0x2E)
		offset = 0x1A0;

	memmove((void *)exec_addr, (void *)(load_addr + offset), count);

	while(!(UART0->FSR & UART_FSR_TXEMPTY));
	for(i = 0; i < 16384; i++)__asm__("nop");
	exec_addr();
	while(1);
}

void __attribute__((section(".init"),naked)) crt0(void){
	__asm__("mov r0, #0x3C000000"); // set STACK to SRAM START + 16k
	__asm__("add r0, #0x4000");
	__asm__("mov sp, r0");
	main();
}

