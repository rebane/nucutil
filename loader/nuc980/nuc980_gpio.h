#ifndef _NUC980_GPIO_H_
#define _NUC980_GPIO_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t MODE;
	volatile uint32_t DINOFF;
	volatile uint32_t DOUT;
	volatile uint32_t DATMSK;
	volatile uint32_t PIN;
	volatile uint32_t DBEN;
	volatile uint32_t INTTYPE;
	volatile uint32_t INTEN;
	volatile uint32_t INTSRC;
	volatile uint32_t SMTEN;
	volatile uint32_t SLEWCTL;
	uint32_t RESERVED1[1];
	volatile uint32_t PUSEL;
}GPIO_TypeDef;

#define GPIOA                  ((GPIO_TypeDef *)0xB0004000)
#define GPIOB                  ((GPIO_TypeDef *)0xB0004040)
#define GPIOC                  ((GPIO_TypeDef *)0xB0004080)
#define GPIOD                  ((GPIO_TypeDef *)0xB00040C0)
#define GPIOE                  ((GPIO_TypeDef *)0xB0004100)
#define GPIOF                  ((GPIO_TypeDef *)0xB0004140)
#define GPIOG                  ((GPIO_TypeDef *)0xB0004180)

// GPIO_MODE
#define GPIO_MODE_MODE0        (((uint32_t)0x03) << 0)
#define GPIO_MODE_MODE1        (((uint32_t)0x03) << 2)
#define GPIO_MODE_MODE2        (((uint32_t)0x03) << 4)
#define GPIO_MODE_MODE3        (((uint32_t)0x03) << 6)
#define GPIO_MODE_MODE4        (((uint32_t)0x03) << 8)
#define GPIO_MODE_MODE5        (((uint32_t)0x03) << 10)
#define GPIO_MODE_MODE6        (((uint32_t)0x03) << 12)
#define GPIO_MODE_MODE7        (((uint32_t)0x03) << 14)
#define GPIO_MODE_MODE8        (((uint32_t)0x03) << 16)
#define GPIO_MODE_MODE9        (((uint32_t)0x03) << 18)
#define GPIO_MODE_MODE10       (((uint32_t)0x03) << 20)
#define GPIO_MODE_MODE11       (((uint32_t)0x03) << 22)
#define GPIO_MODE_MODE12       (((uint32_t)0x03) << 24)
#define GPIO_MODE_MODE13       (((uint32_t)0x03) << 26)
#define GPIO_MODE_MODE14       (((uint32_t)0x03) << 28)
#define GPIO_MODE_MODE15       (((uint32_t)0x03) << 30)

// GPIO_DOUT
#define GPIO_DOUT_DOUT0        (((uint32_t)0x01) << 0)
#define GPIO_DOUT_DOUT1        (((uint32_t)0x01) << 1)
#define GPIO_DOUT_DOUT2        (((uint32_t)0x01) << 2)
#define GPIO_DOUT_DOUT3        (((uint32_t)0x01) << 3)
#define GPIO_DOUT_DOUT4        (((uint32_t)0x01) << 4)
#define GPIO_DOUT_DOUT5        (((uint32_t)0x01) << 5)
#define GPIO_DOUT_DOUT6        (((uint32_t)0x01) << 6)
#define GPIO_DOUT_DOUT7        (((uint32_t)0x01) << 7)
#define GPIO_DOUT_DOUT8        (((uint32_t)0x01) << 8)
#define GPIO_DOUT_DOUT9        (((uint32_t)0x01) << 9)
#define GPIO_DOUT_DOUT10       (((uint32_t)0x01) << 10)
#define GPIO_DOUT_DOUT11       (((uint32_t)0x01) << 11)
#define GPIO_DOUT_DOUT12       (((uint32_t)0x01) << 12)
#define GPIO_DOUT_DOUT13       (((uint32_t)0x01) << 13)
#define GPIO_DOUT_DOUT14       (((uint32_t)0x01) << 14)
#define GPIO_DOUT_DOUT15       (((uint32_t)0x01) << 15)

// GPIO_PIN
#define GPIO_PIN_PIN0          (((uint32_t)0x01) << 0)
#define GPIO_PIN_PIN1          (((uint32_t)0x01) << 1)
#define GPIO_PIN_PIN2          (((uint32_t)0x01) << 2)
#define GPIO_PIN_PIN3          (((uint32_t)0x01) << 3)
#define GPIO_PIN_PIN4          (((uint32_t)0x01) << 4)
#define GPIO_PIN_PIN5          (((uint32_t)0x01) << 5)
#define GPIO_PIN_PIN6          (((uint32_t)0x01) << 6)
#define GPIO_PIN_PIN7          (((uint32_t)0x01) << 7)
#define GPIO_PIN_PIN8          (((uint32_t)0x01) << 8)
#define GPIO_PIN_PIN9          (((uint32_t)0x01) << 9)
#define GPIO_PIN_PIN10         (((uint32_t)0x01) << 10)
#define GPIO_PIN_PIN11         (((uint32_t)0x01) << 11)
#define GPIO_PIN_PIN12         (((uint32_t)0x01) << 12)
#define GPIO_PIN_PIN13         (((uint32_t)0x01) << 13)
#define GPIO_PIN_PIN14         (((uint32_t)0x01) << 14)
#define GPIO_PIN_PIN15         (((uint32_t)0x01) << 15)

// GPIO_PUSEL
#define GPIO_PUSEL_PUSEL0      (((uint32_t)0x03) << 0)
#define GPIO_PUSEL_PUSEL1      (((uint32_t)0x03) << 2)
#define GPIO_PUSEL_PUSEL2      (((uint32_t)0x03) << 4)
#define GPIO_PUSEL_PUSEL3      (((uint32_t)0x03) << 6)
#define GPIO_PUSEL_PUSEL4      (((uint32_t)0x03) << 8)
#define GPIO_PUSEL_PUSEL5      (((uint32_t)0x03) << 10)
#define GPIO_PUSEL_PUSEL6      (((uint32_t)0x03) << 12)
#define GPIO_PUSEL_PUSEL7      (((uint32_t)0x03) << 14)
#define GPIO_PUSEL_PUSEL8      (((uint32_t)0x03) << 16)
#define GPIO_PUSEL_PUSEL9      (((uint32_t)0x03) << 18)
#define GPIO_PUSEL_PUSEL10     (((uint32_t)0x03) << 20)
#define GPIO_PUSEL_PUSEL11     (((uint32_t)0x03) << 22)
#define GPIO_PUSEL_PUSEL12     (((uint32_t)0x03) << 24)
#define GPIO_PUSEL_PUSEL13     (((uint32_t)0x03) << 26)
#define GPIO_PUSEL_PUSEL14     (((uint32_t)0x03) << 28)
#define GPIO_PUSEL_PUSEL15     (((uint32_t)0x03) << 30)

#endif

