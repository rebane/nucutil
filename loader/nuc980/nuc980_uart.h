#ifndef _NUC980_UART_H_
#define _NUC980_UART_H_

#include <stdint.h>

typedef struct{
	union{
		volatile uint32_t DAT;
		volatile uint32_t RBR;
		volatile uint32_t THR;
	};
	union{
		volatile uint32_t INTEN;
		volatile uint32_t IER;
	};
	union{
		volatile uint32_t FIFO;
		volatile uint32_t FCR;
	};
	union{
		volatile uint32_t LINE;
		volatile uint32_t LCR;
	};
	union{
		volatile uint32_t MODEM;
		volatile uint32_t MCR;
	};
	union{
		volatile uint32_t MODEMSTS;
		volatile uint32_t MSR;
	};
	union{
		volatile uint32_t FIFOSTS;
		volatile uint32_t FSR;
	};
	union{
		volatile uint32_t INTSTS;
		volatile uint32_t ISR;
	};
	union{
		volatile uint32_t TOUT;
		volatile uint32_t TOR;
	};
	volatile uint32_t BAUD;
	union{
		volatile uint32_t IRDA;
		volatile uint32_t IRCR;
	};
	union{
		volatile uint32_t ALTCTL;
		volatile uint32_t ALT_CSR;
	};
	union{
		volatile uint32_t FUNCSEL;
		volatile uint32_t FUN_SEL;
	};
	union{
		volatile uint32_t LINCTL;
		volatile uint32_t LIN_CTL;
	};
	union{
		volatile uint32_t LINSTS;
		volatile uint32_t LIN_SR;
	};
	volatile uint32_t BRCOMP;
	volatile uint32_t WKCTL;
	volatile uint32_t WKSTS;
	volatile uint32_t DWKCOMP;
}UART_TypeDef;

#define UART0                ((UART_TypeDef *)0xB0070000)
#define UART1                ((UART_TypeDef *)0xB0071000)
#define UART2                ((UART_TypeDef *)0xB0072000)
#define UART3                ((UART_TypeDef *)0xB0073000)
#define UART4                ((UART_TypeDef *)0xB0074000)
#define UART5                ((UART_TypeDef *)0xB0075000)
#define UART6                ((UART_TypeDef *)0xB0076000)
#define UART7                ((UART_TypeDef *)0xB0077000)
#define UART8                ((UART_TypeDef *)0xB0078000)
#define UART9                ((UART_TypeDef *)0xB0079000)

// UART_DAT
#define UART_DAT_DAT         (((uint32_t)0xff) << 0)
#define UART_DAT_PARITY      (((uint32_t)0x01) << 0)

// UART_INTEN
#define UART_INTEN_RDAIEN    (((uint32_t)0x01) << 0)
#define UART_INTEN_THREIEN   (((uint32_t)0x01) << 1)
#define UART_INTEN_RLSIEN    (((uint32_t)0x01) << 2)
#define UART_INTEN_MODEMIEN  (((uint32_t)0x01) << 3)
#define UART_INTEN_RXTOIEN   (((uint32_t)0x01) << 4)
#define UART_INTEN_BUFERRIEN (((uint32_t)0x01) << 5)
#define UART_INTEN_WKIEN     (((uint32_t)0x01) << 6)
#define UART_INTEN_LINIEN    (((uint32_t)0x01) << 8)
#define UART_INTEN_TOCNTEN   (((uint32_t)0x01) << 11)
#define UART_INTEN_ATORTSEN  (((uint32_t)0x01) << 12)
#define UART_INTEN_ATOCTSEN  (((uint32_t)0x01) << 13)
#define UART_INTEN_TXPDMAEN  (((uint32_t)0x01) << 14)
#define UART_INTEN_RXPDMAEN  (((uint32_t)0x01) << 15)
#define UART_INTEN_ABRIEN    (((uint32_t)0x01) << 18)
#define UART_INTEN_TXENDIEN  (((uint32_t)0x01) << 22)

// UART_FIFO, FCR
#define UART_FIFO_RXRST      (((uint32_t)0x01) << 1)
#define UART_FIFO_TXRST      (((uint32_t)0x01) << 2)
#define UART_FIFO_RFITL      (((uint32_t)0x0f) << 4)
#define UART_FIFO_RXOFF      (((uint32_t)0x01) << 8)
#define UART_FIFO_RTSTRGLV   (((uint32_t)0x0f) << 16)
#define UART_FCR_RXRST       (((uint32_t)0x01) << 1)
#define UART_FCR_TXRST       (((uint32_t)0x01) << 2)
#define UART_FCR_RFITL       (((uint32_t)0x0f) << 4)
#define UART_FCR_RXOFF       (((uint32_t)0x01) << 8)
#define UART_FCR_RTSTRGLV    (((uint32_t)0x0f) << 16)

// UART_LINE, LCR
#define UART_LINE_WLS        (((uint32_t)0x03) << 0)
#define UART_LINE_NSB        (((uint32_t)0x01) << 2)
#define UART_LINE_PBE        (((uint32_t)0x01) << 3)
#define UART_LINE_EPE        (((uint32_t)0x01) << 4)
#define UART_LINE_SPE        (((uint32_t)0x01) << 5)
#define UART_LINE_BCB        (((uint32_t)0x01) << 6)
#define UART_LINE_PSS        (((uint32_t)0x01) << 7)
#define UART_LINE_TXDINV     (((uint32_t)0x01) << 8)
#define UART_LINE_RXDINV     (((uint32_t)0x01) << 9)
#define UART_LCR_WLS         (((uint32_t)0x03) << 0)
#define UART_LCR_NSB         (((uint32_t)0x01) << 2)
#define UART_LCR_PBE         (((uint32_t)0x01) << 3)
#define UART_LCR_EPE         (((uint32_t)0x01) << 4)
#define UART_LCR_SPE         (((uint32_t)0x01) << 5)
#define UART_LCR_BCB         (((uint32_t)0x01) << 6)
#define UART_LCR_PSS         (((uint32_t)0x01) << 7)
#define UART_LCR_TXDINV      (((uint32_t)0x01) << 8)
#define UART_LCR_RXDINV      (((uint32_t)0x01) << 9)

// UART_FIFOSTS, FSR
#define UART_FIFOSTS_RXOVIF   (((uint32_t)0x01) << 0)
#define UART_FIFOSTS_ABRDIF   (((uint32_t)0x01) << 1)
#define UART_FIFOSTS_ABRDTOIF (((uint32_t)0x01) << 2)
#define UART_FIFOSTS_ADDRDETF (((uint32_t)0x01) << 3)
#define UART_FIFOSTS_PEF      (((uint32_t)0x01) << 4)
#define UART_FIFOSTS_FEF      (((uint32_t)0x01) << 5)
#define UART_FIFOSTS_BIF      (((uint32_t)0x01) << 6)
#define UART_FIFOSTS_RXPTR    (((uint32_t)0x3f) << 8)
#define UART_FIFOSTS_RXEMPTY  (((uint32_t)0x01) << 14)
#define UART_FIFOSTS_RXFULL   (((uint32_t)0x01) << 15)
#define UART_FIFOSTS_TXPTR    (((uint32_t)0x3f) << 16)
#define UART_FIFOSTS_TXEMPTY  (((uint32_t)0x01) << 22)
#define UART_FIFOSTS_TXFULL   (((uint32_t)0x01) << 23)
#define UART_FIFOSTS_TXOVIF   (((uint32_t)0x01) << 24)
#define UART_FIFOSTS_TXEMPTYF (((uint32_t)0x01) << 28)
#define UART_FIFOSTS_TXIDLE   (((uint32_t)0x01) << 29)
#define UART_FIFOSTS_TXRXACT  (((uint32_t)0x01) << 31)
#define UART_FSR_RXOVIF       (((uint32_t)0x01) << 0)
#define UART_FSR_ABRDIF       (((uint32_t)0x01) << 1)
#define UART_FSR_ABRDTOIF     (((uint32_t)0x01) << 2)
#define UART_FSR_ADDRDETF     (((uint32_t)0x01) << 3)
#define UART_FSR_PEF          (((uint32_t)0x01) << 4)
#define UART_FSR_FEF          (((uint32_t)0x01) << 5)
#define UART_FSR_BIF          (((uint32_t)0x01) << 6)
#define UART_FSR_RXPTR        (((uint32_t)0x3f) << 8)
#define UART_FSR_RXEMPTY      (((uint32_t)0x01) << 14)
#define UART_FSR_RXFULL       (((uint32_t)0x01) << 15)
#define UART_FSR_TXPTR        (((uint32_t)0x3f) << 16)
#define UART_FSR_TXEMPTY      (((uint32_t)0x01) << 22)
#define UART_FSR_TXFULL       (((uint32_t)0x01) << 23)
#define UART_FSR_TXOVIF       (((uint32_t)0x01) << 24)
#define UART_FSR_TXEMPTYF     (((uint32_t)0x01) << 28)
#define UART_FSR_TXIDLE       (((uint32_t)0x01) << 29)
#define UART_FSR_TXRXACT      (((uint32_t)0x01) << 31)

// UART_BAUD
#define UART_BAUD_BRD         (((uint32_t)0xffff) << 0)
#define UART_BAUD_EDIVM1      (((uint32_t)0x0001) << 24)
#define UART_BAUD_BAUDM0      (((uint32_t)0x0001) << 28)
#define UART_BAUD_BAUDM1      (((uint32_t)0x0001) << 29)

#endif

