#ifndef _FILE_H_
#define _FILE_H_

#include <stdint.h>

#define FILE_TYPE_SPINOR  1
#define FILE_TYPE_SPINAND 2
#define FILE_TYPE_NAND    3
#define FILE_TYPE_SD      4

struct file_struct{
	int count;
	void *buf;
};

typedef struct file_struct file_t;

file_t *file_open(char *path, int offset, int count);
int file_write_boot(file_t *f, char *path, uint32_t exec_addr, int type, void *ddr_buf, int ddr_count);
void file_close(file_t *f);

#endif

