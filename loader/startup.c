#include <stdint.h>
#include "nuc980.h"

extern uint8_t __bss_beg__, __bss_end__;

extern int main(int argc, char *argv[]);

void __attribute__((section(".init"),naked)) crt0(void){
	volatile uint8_t *m;

	__asm__("b start");
	__asm__("pwron1: .word 0x00000000");
	__asm__(".global pwron1");
	__asm__("pwron2: .word 0x00000000");
	__asm__(".global pwron2");
	__asm__("boot_cmd: .word 0x00000000");
	__asm__(".global boot_cmd");
	__asm__("boot_arg: .word 0x00000000");
	__asm__(".global boot_arg");
	__asm__("start:");

	AIC->INTDIS0 = 0xFFFFFFFF;
	AIC->INTDIS1 = 0xFFFFFFFF;

	__asm__("mov r0, %0": : "i" (STACK_ADDR));
	__asm__("mov sp, r0");

	for(m = &__bss_beg__; m < &__bss_end__; m++){
		*m = 0;
	}

	main(0, (void *)0);
}

