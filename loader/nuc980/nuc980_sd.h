#ifndef _NUC980_SD_H_
#define _NUC980_SD_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t FB0;
	volatile uint32_t FB1;
	volatile uint32_t FB2;
	volatile uint32_t FB3;
	volatile uint32_t FB4;
	uint32_t RESERVED1[251];
	volatile uint32_t DMACCSR;
	uint32_t RESERVED2[1];
	volatile uint32_t DMACSAR;
	volatile uint32_t DMACBCR;
	volatile uint32_t DMACIER;
	volatile uint32_t DMACISR;
	uint32_t RESERVED3[250];
	volatile uint32_t FMICSR; // EMMC_CTL
	volatile uint32_t FMIIER;
	volatile uint32_t FMIISR;
	uint32_t RESERVED4[5];
	volatile uint32_t SDCSR;  // FMI_EMMCCTL
	volatile uint32_t SDARG;
	volatile uint32_t SDIER;
	volatile uint32_t SDISR;
	volatile uint32_t SDRSP0;
	volatile uint32_t SDRSP1;
	volatile uint32_t SDBLEN;
	volatile uint32_t SDTOUT;
	volatile uint32_t SDECR;
}SD_TypeDef;

#define SD0                  ((SD_TypeDef *)0xB0019000)

// SD_DMACCSR
#define SD_DMACCSR_DMACEN    (((uint32_t)0x01) << 0)
#define SD_DMACCSR_SWRST     (((uint32_t)0x01) << 1)

// SD_DMACIER
#define SD_DMACIER_TABORTIE  (((uint32_t)0x01) << 0)

// SD_FMICSR
#define SD_FMICSR_SWRST      (((uint32_t)0x01) << 0)
#define SD_FMICSR_SDEN       (((uint32_t)0x01) << 1)

// SD_FMIIER
#define SD_FMIIER_DTAIE      (((uint32_t)0x01) << 0)

// SD_SDCSR
#define SD_SDCSR_COEN        (((uint32_t)0x01) << 0)
#define SD_SDCSR_RIEN        (((uint32_t)0x01) << 1)
#define SD_SDCSR_DIEN        (((uint32_t)0x01) << 2)
#define SD_SDCSR_DOEN        (((uint32_t)0x01) << 3)
#define SD_SDCSR_R2EN        (((uint32_t)0x01) << 4)
#define SD_SDCSR_CLK74_OE    (((uint32_t)0x01) << 5)
#define SD_SDCSR_CLK8_OE     (((uint32_t)0x01) << 6)
#define SD_SDCSR_CLK_KEEP0   (((uint32_t)0x01) << 7)
#define SD_SDCSR_CMD         (((uint32_t)0x3F) << 8)
#define SD_SDCSR_CTLRST      (((uint32_t)0x01) << 14)
#define SD_SDCSR_DBW         (((uint32_t)0x01) << 15)
#define SD_SDCSR_BLK_CNT     (((uint32_t)0xFF) << 16)
#define SD_SDCSR_NWR         (((uint32_t)0x0F) << 24)

// SD_SDIER
#define SD_SDIER_BLKDIE      (((uint32_t)0x01) << 0)
#define SD_SDIER_CD0IE       (((uint32_t)0x01) << 8)
#define SD_SDIER_SDIO0IE     (((uint32_t)0x01) << 10)
#define SD_SDIER_RITOIE      (((uint32_t)0x01) << 12)
#define SD_SDIER_DITOIE      (((uint32_t)0x01) << 13)
#define SD_SDIER_WKUPIE      (((uint32_t)0x01) << 14)
#define SD_SDIER_DONEIE      (((uint32_t)0x01) << 24)
#define SD_SDIER_CD0SRC      (((uint32_t)0x01) << 30)

// SD_SDISR
#define SD_SDISR_BLKDIF      (((uint32_t)0x01) << 0)
#define SD_SDISR_CRCIF       (((uint32_t)0x01) << 1)
#define SD_SDISR_CRC7        (((uint32_t)0x01) << 2)
#define SD_SDISR_SDDAT0      (((uint32_t)0x01) << 7)
#define SD_SDISR_CD0IF       (((uint32_t)0x01) << 8)
#define SD_SDISR_SDIO0IF     (((uint32_t)0x01) << 10)
#define SD_SDISR_RITOIF      (((uint32_t)0x01) << 12)
#define SD_SDISR_DITOIF      (((uint32_t)0x01) << 13)
#define SD_SDISR_CDPS0       (((uint32_t)0x01) << 16)
#define SD_SDISR_DONEIF      (((uint32_t)0x01) << 24)

#endif

