#ifndef _DDR_H_
#define _DDR_H_

struct ddr_struct{
	int count;
	int data_count;
	void *buf;
	void *data;
	char *xusb_path;
};

typedef struct ddr_struct ddr_t;

ddr_t *ddr_open(char *path, char *search);
void ddr_close(ddr_t *d);

#endif

