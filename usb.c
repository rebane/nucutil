#include "usb.h"
#include <stdlib.h>
#include <string.h>
#include <libusb-1.0/libusb.h>

static int usb_initialized = 0;

struct usb_device_handle_struct {
	libusb_device_handle *lusb_dev_handle;
};

int usb_init()
{
	if (usb_initialized)
		return usb_initialized;

	if (libusb_init(NULL) < 0)
		usb_initialized = -1;
	else
		usb_initialized = 1;

	if (usb_initialized < 0)
		return -1;

	return 0;
}

usb_device_list_t *usb_get_device_list(uint16_t vid, uint16_t pid)
{
	struct libusb_device_descriptor desc;
	libusb_device **devs;
	usb_device_list_t *ul;
	size_t i, cnt, num;
	int ret;
	void *p;

	ul = malloc(sizeof(usb_device_list_t));
	if (ul == NULL)
		return NULL;

	cnt = libusb_get_device_list(NULL, &devs);
	if (cnt < 0) {
		free(ul);
		return NULL;
	}
	num = 0;
	ul->count = 0;
	ul->path = NULL;
	for (i = 0; i < cnt; i++) {
		ret = libusb_get_device_descriptor(devs[i], &desc);
		if (ret < 0)
			continue;

		if ((desc.idVendor == vid) && (desc.idProduct == pid)) {
			if (ul->count >= num) {
				num = num + 4;
				p = realloc(ul->path, num * sizeof(usb_path_t));
				if (p == NULL)
					break;

				ul->path = p;
			}
			ul->path[ul->count].count = libusb_get_port_numbers(devs[i], ul->path[ul->count].port, USB_PORTS_MAX);
			if (ul->path[ul->count].count < 0)
				continue;

			ul->count++;
		}
	}
	libusb_free_device_list(devs, 1);
	return ul;
}

void usb_free_device_list(usb_device_list_t *ul)
{
	free(ul->path);
	free(ul);
}

usb_path_t *usb_parse_path(char *pathstring)
{
	usb_path_t *path;
	char *s;

	if (pathstring == NULL)
		return NULL;

	path = malloc(sizeof(usb_path_t));
	if (path == NULL)
		return NULL;

	for (path->count = 0; path->count < USB_PORTS_MAX; path->count++) {
		path->port[path->count] = strtoul(pathstring, NULL, 0);
		s = strchr(pathstring, '-');
		if (s == NULL) {
			path->count++;
			break;
		}
		pathstring = &s[1];
	}
	return path;
}

void usb_free_path(usb_path_t *path)
{
	free(path);
}

usb_device_handle_t *usb_open(uint16_t vid, uint16_t pid, usb_path_t *path)
{
	libusb_device **devs;
	struct libusb_device_descriptor desc;
	usb_path_t usb_path;
	usb_device_handle_t *dh;
	int i, cnt, ret;

	cnt = libusb_get_device_list(NULL, &devs);
	if (cnt < 0)
		return NULL;

	for (i = 0; i < cnt; i++) {
		ret = libusb_get_device_descriptor(devs[i], &desc);
		if (ret < 0)
			continue;

		if ((desc.idVendor == vid) && (desc.idProduct == pid)) {
			usb_path.count = libusb_get_port_numbers(devs[i], usb_path.port, USB_PORTS_MAX);
			if (usb_path.count < 0)
				continue;

			if ((usb_path.count == path->count) && !memcmp(usb_path.port, path->port, path->count))
				break;
		}
	}
	if (i >= cnt) {
		libusb_free_device_list(devs, 1);
		return NULL;
	}
	dh = malloc(sizeof(usb_device_handle_t));
	if (dh == NULL)
		return NULL;

	ret = libusb_open(devs[i], &dh->lusb_dev_handle);
	if (ret < 0) {
		free(dh);
		return NULL;
	}
	return dh;
}

int usb_claim_interface(usb_device_handle_t *dh, int interface)
{
	int ret;

	ret = libusb_claim_interface(dh->lusb_dev_handle, interface);
	if (ret != LIBUSB_SUCCESS)
		return -1;

	return 0;
}

int usb_release_interface(usb_device_handle_t *dh, int interface)
{
	int ret;

	ret = libusb_release_interface(dh->lusb_dev_handle, interface);
	if (ret != LIBUSB_SUCCESS)
		return -1;

	return 0;
}

int usb_reset_device(usb_device_handle_t *dh)
{
	int ret;

	ret = libusb_reset_device(dh->lusb_dev_handle);
	if (ret != LIBUSB_SUCCESS)
		return -1;

	return 0;
}

int usb_control_transfer(usb_device_handle_t *dh, uint8_t bm_requestType, uint8_t b_request, uint16_t w_value, uint16_t w_index, void *data, uint16_t w_length, unsigned int timeout)
{
	int ret;

	ret = libusb_control_transfer(dh->lusb_dev_handle, bm_requestType, b_request, w_value, w_index, data, w_length, timeout);
	if (ret < 0)
		return -1;

	return ret;
}

int usb_bulk_transfer(usb_device_handle_t *dh, uint8_t endpoint, void *data, int length, int *transferred, unsigned int timeout)
{
	int ret;

	ret = libusb_bulk_transfer(dh->lusb_dev_handle, endpoint, data, length, transferred, timeout);
	if (ret < 0)
		return -1;

	return ret;
}

void usb_close(usb_device_handle_t *dh)
{
	libusb_close(dh->lusb_dev_handle);
	free(dh);
}

