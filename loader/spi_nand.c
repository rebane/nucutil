#include "spi_nand.h"
#include <stdio.h>

static uint8_t spi_nand_status(spi_nand_t *nand, uint8_t reg);
static void spi_nand_status_set(spi_nand_t *nand, uint8_t reg, uint8_t value);
static void spi_nand_wait_busy(spi_nand_t *nand);
static void spi_nand_write_enable(spi_nand_t *nand);
static int spi_nand_cache_page_read(spi_nand_t *nand, uint32_t offset);
static int spi_nand_erase_block(spi_nand_t *nand, uint32_t offset);
static int spi_nand_write_page(spi_nand_t *nand, uint32_t offset, void *src);
static int spi_nand_mark_bad_block(spi_nand_t *nand, uint32_t offset);

void spi_nand_init(spi_nand_t *nand, void *user, spi_nand_transfer_half_t transfer_half, int cs){
	uint8_t buffer[3];

	nand->transfer_half = transfer_half;
	nand->user = user;
	nand->cs = cs;

	nand->cache_page = 0xFFFFFFFF;
	nand->transfer_half(nand->user, nand->cs, "\x9F\x00", 2, buffer, 3);
	nand->id = ((uint32_t)buffer[0] << 16) | ((uint32_t)buffer[1] << 8) | buffer[2];
	printf("SPI NAND: ID: 0x%06x\n", (unsigned int)nand->id);
	spi_nand_status_set(nand, 2, spi_nand_status(nand, 2) | (1 << 4) | (1 << 3)); // enable ECC & BUF mode
//	spi_nand_mark_bad_block(nand, 0x1140000);
//	spi_nand_mark_bad_block(nand, 0x6e0000);
//	spi_nand_erase_block(nand, 0x1140000);
//	spi_nand_erase_block(nand, 0x6e0000);
}

void spi_nand_part_init(spi_nand_partition_t *snp, spi_nand_t *nand, uint32_t offset, uint32_t size){
	uint32_t block_offset;
	int i;
	snp->nand = nand;
	snp->offset = offset;
	snp->size = size;
	snp->block_count = (snp->size / SPI_NAND_BLOCK_SIZE);
	snp->last_virtual_block = 0xFFFFFFFF;
	for(i = 0; i < snp->block_count; i++){
		block_offset = snp->offset + (i * SPI_NAND_BLOCK_SIZE);
		spi_nand_cache_page_read(nand, block_offset);
		if(nand->cache[SPI_NAND_PAGE_SIZE + 0] != 0xFF){ // first byte in OOB
			snp->block_list[i] = 1;
			printf("SPI NAND: Bad block at 0x%08x\n", (unsigned int)block_offset);
		}else{
			snp->block_list[i] = 0;
		}
	}
}

static uint8_t spi_nand_status(spi_nand_t *nand, uint8_t reg){
	uint8_t c;
	if(reg == 1){
		nand->transfer_half(nand->user, nand->cs, "\x05\xA0", 2, &c, 1);
	}else if(reg == 2){
		nand->transfer_half(nand->user, nand->cs, "\x05\xB0", 2, &c, 1);
	}else if(reg == 3){
		nand->transfer_half(nand->user, nand->cs, "\x05\xC0", 2, &c, 1);
	}
	return(c);
}

static void spi_nand_status_set(spi_nand_t *nand, uint8_t reg, uint8_t value){
	uint8_t buffer[3];
	buffer[0] = 0x01;
	if(reg == 1){
		buffer[1] = 0xA0;
	}else if(reg == 2){
		buffer[1] = 0xB0;
	}else if(reg == 3){
		buffer[1] = 0xC0;
	}
	buffer[2] = value;
	nand->transfer_half(nand->user, nand->cs, buffer, 3, buffer, 0);
}

static void spi_nand_wait_busy(spi_nand_t *nand){
	while(spi_nand_status(nand, 3) & 0x01);
}

static void spi_nand_write_enable(spi_nand_t *nand){
	nand->transfer_half(nand->user, nand->cs, "\x06", 1, (void *)0, 0);
}

static int spi_nand_cache_page_read(spi_nand_t *nand, uint32_t offset){
	uint8_t buffer[4], ecc;
	uint32_t page = (offset / SPI_NAND_PAGE_SIZE);
	if(nand->cache_page == page)return(nand->cache_status);

	buffer[0] = 0x13;
	buffer[1] = 0x00;
	buffer[2] = (page >> 8) & 0xFF;
	buffer[3] = (page >> 0) & 0xFF;
	nand->transfer_half(nand->user, nand->cs, buffer, 4, buffer, 0);
	spi_nand_wait_busy(nand);

	buffer[0] = 0x0B;
	buffer[1] = (0 >> 8) & 0xFF;
	buffer[2] = (0 >> 0) & 0xFF;
	buffer[3] = 0x00;
	nand->transfer_half(nand->user, nand->cs, buffer, 4, nand->cache, SPI_NAND_CACHE_SIZE);

	nand->cache_page = page;
	ecc = (spi_nand_status(nand, 3) >> 4) & 0x03;
	if((ecc == 0) || (ecc == 1)){
		nand->cache_status = 0;
	}else{
		nand->cache_status = -1;
	}
	return(nand->cache_status);
	
}

static int spi_nand_erase_block(spi_nand_t *nand, uint32_t offset){
	uint8_t buffer[4];
	uint32_t page = (offset / SPI_NAND_PAGE_SIZE);

	spi_nand_write_enable(nand);
	buffer[0] = 0xD8;
	buffer[1] = 0x00;
	buffer[2] = (page >> 8) & 0xFF;
	buffer[3] = (page >> 0) & 0xFF;
	nand->transfer_half(nand->user, nand->cs, buffer, 4, buffer, 0);
	spi_nand_wait_busy(nand);
	if((spi_nand_status(nand, 3) >> 2) & 0x03)return(-1);
	return(0);
}

static int spi_nand_write_page(spi_nand_t *nand, uint32_t offset, void *src){
	uint8_t buffer[3 + SPI_NAND_PAGE_SIZE];
	uint32_t page = (offset / SPI_NAND_PAGE_SIZE);

	spi_nand_write_enable(nand);
	buffer[0] = 0x02;
	buffer[1] = (0 >> 8) & 0xFF;
	buffer[2] = (0 >> 0) & 0xFF;
	memcpy(&buffer[3], src, SPI_NAND_PAGE_SIZE);
	nand->transfer_half(nand->user, nand->cs, buffer, 3 + SPI_NAND_PAGE_SIZE, buffer, 0);
	spi_nand_wait_busy(nand);

	spi_nand_write_enable(nand);
	buffer[0] = 0x10;
	buffer[1] = 0x00;
	buffer[2] = (page >> 8) & 0xFF;
	buffer[3] = (page >> 0) & 0xFF;
	nand->transfer_half(nand->user, nand->cs, buffer, 4, buffer, 0);
	spi_nand_wait_busy(nand);
	if((spi_nand_status(nand, 3) >> 2) & 0x03)return(-1);
	return(0);
}

static int spi_nand_mark_bad_block(spi_nand_t *nand, uint32_t offset){
	uint8_t buffer[4];
	uint32_t page = (offset / SPI_NAND_PAGE_SIZE);

	spi_nand_write_enable(nand);
	buffer[0] = 0x02;
	buffer[1] = (SPI_NAND_PAGE_SIZE >> 8) & 0xFF; // first byte in OOB
	buffer[2] = (SPI_NAND_PAGE_SIZE >> 0) & 0xFF;
	buffer[3] = 0xF0;
	nand->transfer_half(nand->user, nand->cs, buffer, 4, buffer, 0);
	spi_nand_wait_busy(nand);

	spi_nand_write_enable(nand);
	buffer[0] = 0x10;
	buffer[1] = 0x00;
	buffer[2] = (page >> 8) & 0xFF;
	buffer[3] = (page >> 0) & 0xFF;
	nand->transfer_half(nand->user, nand->cs, buffer, 4, buffer, 0);
	spi_nand_wait_busy(nand);
	if((spi_nand_status(nand, 3) >> 2) & 0x03)return(-1);
	return(0);
}

int32_t spi_nand_part_copy(spi_nand_partition_t *dst_snp, spi_nand_partition_t *src_snp, int32_t count){
	uint8_t buffer[SPI_NAND_BLOCK_SIZE];
	// TODO: verify before erasing & copying, maybe it is not needed
	uint32_t i, j, dst_block = 0;
	for(i = 0; i < count; i += SPI_NAND_BLOCK_SIZE){
		if(spi_nand_part_read(src_snp, buffer, i, SPI_NAND_BLOCK_SIZE) != SPI_NAND_BLOCK_SIZE){
			printf("SPI NAND: read error\n");
			return(-1);
		}
		do{
			for( ; dst_block < dst_snp->block_count; dst_block++){
				if(dst_snp->block_list[dst_block] == 0)break;
			}
			if(dst_block >= dst_snp->block_count)return(-1);
			if(spi_nand_erase_block(dst_snp->nand, dst_snp->offset + (dst_block * SPI_NAND_BLOCK_SIZE)) < 0){
				spi_nand_mark_bad_block(dst_snp->nand, dst_snp->offset + (dst_block * SPI_NAND_BLOCK_SIZE));
				dst_snp->block_list[dst_block] = 1;
				dst_block++;
				continue;
			}
			for(j = 0; j < SPI_NAND_BLOCK_SIZE; j += SPI_NAND_PAGE_SIZE){
				if(spi_nand_write_page(dst_snp->nand, dst_snp->offset + (dst_block * SPI_NAND_BLOCK_SIZE) + j, &buffer[j]) < 0){
					break;
				}
			}
			if(j < SPI_NAND_BLOCK_SIZE){
				spi_nand_mark_bad_block(dst_snp->nand, dst_snp->offset + (dst_block * SPI_NAND_BLOCK_SIZE));
				dst_snp->block_list[dst_block] = 1;
				dst_block++;
				continue;
			}
			dst_block++;
		}while(0);
	}
	return(count);
}

uint32_t spi_nand_part_phy_addr(spi_nand_partition_t *snp, uint32_t part_offset){
	int i;
	uint32_t block = (part_offset / SPI_NAND_BLOCK_SIZE);
	uint32_t good_blocks = 0;
	if(snp->last_virtual_block != block){
		snp->last_virtual_block = block;
		for(i = 0; i < snp->block_count; i++){
			if(!snp->block_list[i]){
				if(snp->last_virtual_block == good_blocks){
					snp->last_physical_block = (i * SPI_NAND_BLOCK_SIZE);
					break;
				}
				good_blocks++;
			}
		}
		if(i >= snp->block_count)snp->last_physical_block = 0xFFFFFFFF;
	}
	if(snp->last_physical_block == 0xFFFFFFFF)return(0xFFFFFFFF);
	return(snp->offset + snp->last_physical_block + (part_offset % SPI_NAND_BLOCK_SIZE));
}

int32_t spi_nand_part_read(spi_nand_partition_t *snp, void *dest, uint32_t src_part_offset, int32_t count){
	uint32_t i, offset;
	for(i = 0; i < count; i++){
		offset = spi_nand_part_phy_addr(snp, src_part_offset + i);
		if(offset == 0xFFFFFFFF)return(-1);
		if(spi_nand_cache_page_read(snp->nand, offset) < 0)return(-1);
		((uint8_t *)dest)[i] = snp->nand->cache[(src_part_offset + i) % SPI_NAND_PAGE_SIZE];
	}
	return(count);
}

