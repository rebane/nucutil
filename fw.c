#include "fw.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

fw_t *fw_open(char *path, char *search)
{
	fw_t *fw;
	int i, fd;
	char line[512];
	struct stat s;

	if (path == NULL)
		return NULL;

	fw = malloc(sizeof(fw_t));
	if (fw == NULL)
		return NULL;

	fd = open(path, O_RDONLY);
	if (fd < 0) {
		if (search != NULL) {
			i = snprintf(line, 511, "%s/%s", search, path);
			if (i > 511)
				i = 511;
			line[i] = 0;
			fd = open(line, O_RDONLY);
		}
	}
	if (fd < 0) {
		free(fw);
		return NULL;
	}
	if (fstat(fd, &s) < 0) {
		close(fd);
		free(fw);
		return NULL;
	}
	if ((s.st_size < 16) || (s.st_size > 65536)) {
		close(fd);
		free(fw);
		return NULL;
	}
	fw->data = malloc(s.st_size);
	if (fw->data == NULL) {
		close(fd);
		free(fw);
		return NULL;
	}
	if (read(fd, fw->data, s.st_size) != s.st_size) {
		close(fd);
		fw_close(fw);
		return NULL;
	}
	close(fd);
	if (memcmp(fw->data, "\x57\x42\x5A\xA5", 4)) {
		fw_close(fw);
		return NULL ;
	}
	fw->count = ((uint32_t)((uint8_t *)fw->data)[8] << 0) | ((uint32_t)((uint8_t *)fw->data)[9] << 8) | ((uint32_t)((uint8_t *)fw->data)[10] << 16) | ((uint32_t)((uint8_t *)fw->data)[11] << 24);
	if (fw->count != (s.st_size - 16)) {
		fw_close(fw);
		return NULL;
	}
	fw->address = ((uint32_t)((uint8_t *)fw->data)[4] << 0) | ((uint32_t)((uint8_t *)fw->data)[5] << 8) | ((uint32_t)((uint8_t *)fw->data)[6] << 16) | ((uint32_t)((uint8_t *)fw->data)[7] << 24);
	fw->version = ((uint32_t)((uint8_t *)fw->data)[12] << 0) | ((uint32_t)((uint8_t *)fw->data)[13] << 8) | ((uint32_t)((uint8_t *)fw->data)[14] << 16) | ((uint32_t)((uint8_t *)fw->data)[15] << 24);
	fw->buf = &((uint8_t *)fw->data)[16];
	return fw;
}

void fw_close(fw_t *fw)
{
	free(fw->data);
	free(fw);
}

