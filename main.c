#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include "loader/incbin.h"
#include "usb.h"
#include "ddr.h"
#include "fw.h"
#include "nuc.h"
#include "file.h"

#define TYPE_DRAM    1
#define TYPE_SPINOR  2
#define TYPE_SPINAND 3
#define TYPE_NAND    4
#define TYPE_SD      5

INCLUDE_BINARY_FILE(loader_start, loader_end, "loader/loader.bin", ".data", "");

static nuc_info_t info;

static uint32_t address = 0xFFFFFFFF;
static uint32_t length = 0xFFFFFFFF;
static uint32_t pwron;
static int type = 0;
static int run = 0;
static int pwron_set = 0;

static char config_buffer[512];
static char *config_path = NULL;
static char *outfile_path = NULL;

static char *usb_path = NULL;
static nuc_t *nuc = NULL;

static char *ddr_path = NULL;
static ddr_t *ddr = NULL;

static char *fw_path = NULL;
static fw_t *fw = NULL;

static char *file_path = NULL;
static file_t *file = NULL;

static void progress(void *user, int processed, int count);
static int config_parse();
static int interface_open();
static void interface_close();

static int command_list();
static int command_convert();
static int command_info();
static int command_write();
static int command_boot();
static int command_reboot();
static int command_nandfix();

void help(){
	printf("parameters:\n");
	printf("  h - help\n");
	printf("  C - config path\n");
	printf("  d - DDR config file\n");
	printf("  F - ROM helper firmware file\n");
	printf("  f - image file\n");
	printf("  u - USB path\n");
	printf("  o - output file\n");
	printf("  t - set memory type (sdram, spinor, spinand, nand, sd)\n");
	printf("  a - address of memory\n");
	printf("  r - run code (only if memory type is sdram)\n");
	printf("  p - PWRON setting for emulation\n");
	printf("  c - command:\n");
	printf("      list    - list connected NUC980 CPU's with ROM code running\n");
	printf("      convert - adds NUC980 bootable header to the binary image\n");
	printf("      info    - ...\n");
	printf("      write   - writes image file to the memory specified by -t to the offset specified by -a\n");
	printf("      boot    - boots NUC980 with emulated PWRON setting\n");
	printf("      reboot  - reboots the system\n");
	printf("      nandfix - sets a spi nand flash to the known state\n");
	printf("\n");
}

int main(int argc, char *argv[])
{
	int c;

	if (argc < 2) {
		fprintf(stderr, "invalid command line, use -h for help\n");
		return 1;
	}

	opterr = 0;
	while ((c = getopt(argc, argv, "-hC:d:F:f:u:o:t:a:l:rp:c:")) != -1) {
		if (c == 'h') {
			help();
			return 0;
		} else if(c == 'C') {
			config_path = optarg;
			if (!strlen(config_path))
				config_path = NULL;
		} else if(c == 'd') {
			ddr_path = optarg;
			if (ddr != NULL) {
				ddr_close(ddr);
				ddr = NULL;
			}
		} else if(c == 'F') {
			fw_path = optarg;
			if (fw != NULL) {
				fw_close(fw);
				fw = NULL;
			}
		} else if(c == 'f') {
			file_path = optarg;
			if (file != NULL) {
				file_close(file);
				file = NULL;
			}
		} else if(c == 'u') {
			usb_path = optarg;
			if (!strlen(usb_path))
				usb_path = NULL;

			interface_close();
		} else if(c == 'o') {
			outfile_path = optarg;
		} else if(c == 't') {
			if (!strcmp(optarg, "sdram"))
				type = TYPE_DRAM;
			else if(!strcmp(optarg, "spinor"))
				type = TYPE_SPINOR;
			else if(!strcmp(optarg, "spinand"))
				type = TYPE_SPINAND;
			else if(!strcmp(optarg, "nand"))
				type = TYPE_NAND;
			else if(!strcmp(optarg, "sd"))
				type = TYPE_SD;
			else
				type = 0;
		} else if(c == 'a') {
			address = strtoul(optarg, NULL, 0);
		} else if(c == 'l') {
			length = strtoul(optarg, NULL, 0);
		} else if(c == 'r') {
			run = 1;
		} else if(c == 'p') {
			pwron = strtoul(optarg, NULL, 0);
			pwron_set = 1;
		} else if(c == 'c') {
			if (!strcmp(optarg, "list")) {
				if (command_list() < 0) {
					fprintf(stderr, "command_list\n");
					return 1;
				}
			} else if(!strcmp(optarg, "convert")) {
				if (command_convert() < 0) {
					fprintf(stderr, "command_convert\n");
					return 1;
				}
			} else if(!strcmp(optarg, "info")) {
				interface_close();
				if (command_info() < 0) {
					fprintf(stderr, "command_info\n");
					return 1;
				}
			} else if(!strcmp(optarg, "write")) {
				interface_close();
				if (command_write() < 0) {
					fprintf(stderr, "command_write\n");
					return 1;
				}
			} else if(!strcmp(optarg, "boot")) {
				interface_close();
				if (command_boot() < 0) {
					fprintf(stderr, "command_boot\n");
					return 1;
				}
			} else if(!strcmp(optarg, "reboot")) {
				interface_close();
				if (command_reboot() < 0) {
					fprintf(stderr, "command_reboot\n");
					return 1;
				}
			} else if(!strcmp(optarg, "nandfix")) {
				interface_close();
				if (command_nandfix() < 0) {
					fprintf(stderr, "command_nandfix\n");
					return 1;
				}
			}
		} else {
			fprintf(stderr, "invalid command line, use -h for help\n");
			return 1;
		}
	}

	interface_close();
	return 0;
}

static void progress(void *user, int processed, int count)
{
	printf("\r%s: %d%%", (char *)user, ((processed * 100) / count));
	fflush(stdout);
}

static int config_parse()
{
	int i;

	if (config_path != NULL)
		return 0;

	i = readlink("/proc/self/exe", config_buffer, 512);
	if (i > 511)
		i = 511;

	config_buffer[i] = 0;
	config_path = strdup(dirname(config_buffer));
	if (config_path == NULL) {
		fprintf(stderr, "out of memory\n");
		return -1;
	}
	i = snprintf(config_buffer, 511,
			"%s/nuvoton.nuc980_nuwriter_cmd/nudata", config_path);
	if(i > 511)
		i = 511;

	config_buffer[i] = 0;
	free(config_path);
	config_path = config_buffer;

	return 0;
}

static int interface_open()
{
	usb_device_list_t *ul;
	usb_path_t *up;
	int try;

	if (nuc != NULL)
		return 0;

	if (usb_init() < 0) {
		fprintf(stderr, "usb_init\n");
		return -1;
	}

	if (ddr == NULL) {
		if (ddr_path == NULL) {
			fprintf(stderr, "missing ddr file\n");
			return -1;
		}
		ddr = ddr_open(ddr_path, config_path);
		if (ddr == NULL) {
			fprintf(stderr, "ddr_open: %s\n", ddr_path);
			return -1;
		}
	}

	if (fw == NULL) {
		if (fw_path == NULL)
			fw_path = ddr->xusb_path;

		fw = fw_open(fw_path, config_path);
		if (fw == NULL) {
			fprintf(stderr, "fw_open: %s\n", fw_path);
			return -1;
		}
	}

	for (try = 0; try < 32; try++) {
		if (usb_path == NULL) {
			ul = usb_get_device_list(NUC_VENDOR_ID, NUC_PRODUCT_ID);
			if (ul == NULL) {
				fprintf(stderr, "usb_get_device_list\n");
				return -1;
			}
			if (ul->count < 1) {
				if (try > 30) {
					fprintf(stderr, "no devices\n");
					return -1;
				}
				nuc = NULL;
			} else {
				nuc = nuc_open(&ul->path[0]);
			}
		} else {
			up = usb_parse_path(usb_path);
			if (up == NULL) {
				fprintf(stderr, "usb_parse_path: %s\n", usb_path);
				return -1;
			}
			nuc = nuc_open(up);
		}
		if (nuc != NULL)
			break;

		usleep(100000);
	}

//	if (try > 0)
//		printf("tried several times\n");

	if (nuc == NULL) {
		fprintf(stderr, "nuc_open\n");
		return -1;
	}
	if (nuc_ddr(nuc, ddr->buf, ddr->count) < 0) {
		fprintf(stderr, "nuc_ddr\n");
		return -1;
	}
	if (nuc_fw(nuc, fw->address, fw->buf, fw->count) < 0) {
		fprintf(stderr, "nuc_fw\n");
		return -1;
	}
	if (nuc_info(nuc, &info) < 0) {
		fprintf(stderr, "NUC INFO\n");
		return -1;
	}

	return 0;
}

static void interface_close()
{
	if (nuc == NULL)
		return;
	
	nuc_close(nuc);
	nuc = NULL;
}

static int command_list()
{
	usb_device_list_t *ul;
	int i, j;

	if (usb_init() < 0)
		return -1;

	ul = usb_get_device_list(NUC_VENDOR_ID, NUC_PRODUCT_ID);
	if (ul == NULL) {
		fprintf(stderr, "usb_get_device_list\n");
		return -1;
	}
	printf("USB device list:\n");
	for (i = 0; i < ul->count; i++) {
		printf("%d) ", i);
		for (j = 0; j < ul->path[i].count; j++) {
			if (j)
				printf("-%u", (unsigned int)ul->path[i].port[j]);
			else
				printf("%u", (unsigned int)ul->path[i].port[j]);
		}
		printf("\n");
	}

	usb_free_device_list(ul);

	return 0;
}

static int command_convert()
{
	int c;

	if (outfile_path == NULL) {
		fprintf(stderr, "missing output file\n");
		return -1;
	}

	if ((type == 0) || (type == TYPE_DRAM)) {
		fprintf(stderr, "invalid type\n");
		return -1;
	}

	if (address == 0xFFFFFFFF) {
		fprintf(stderr, "invalid address\n");
		return -1;
	}

	if (config_parse() < 0) {
		fprintf(stderr, "config_parse\n");
		return 1;
	}

	if (ddr == NULL) {
		if (ddr_path == NULL) {
			fprintf(stderr, "missing ddr file\n");
			return -1;
		}
		ddr = ddr_open(ddr_path, config_path);
		if (ddr == NULL) {
			fprintf(stderr, "ddr_open: %s\n", ddr_path);
			return -1;
		}
	}

	if (file == NULL) {
		if (file_path == NULL) {
			fprintf(stderr, "missing input file\n");
			return -1;
		}
		file = file_open(file_path, 0, -1);
		if (file == NULL) {
			fprintf(stderr, "file_open: %s\n", file_path);
			return -1;
		}
	}

	if (type == TYPE_SPINOR) {
		c = file_write_boot(file, outfile_path, address,
				FILE_TYPE_SPINOR, ddr->data, ddr->data_count);
	} else if(type == TYPE_SPINAND) {
		c = file_write_boot(file, outfile_path, address,
				FILE_TYPE_SPINAND, ddr->data, ddr->data_count);
	} else if(type == TYPE_NAND) {
		c = file_write_boot(file, outfile_path, address,
				FILE_TYPE_NAND, ddr->data, ddr->data_count);
	} else if(type == TYPE_SD) {
		// boot firmware must be written to the sector 2 (starting from 1024 bytes) on the SD card
		c = file_write_boot(file, outfile_path, address,
				FILE_TYPE_SD, ddr->data, ddr->data_count);
	} else {
		fprintf(stderr, "invalid type\n");
		return -1;
	}

	if (c < 0) {
		fprintf(stderr, "file_write_boot: %s\n", outfile_path);
		return -1;
	}

	return 0;
}

static int command_info()
{
	if (config_parse() < 0) {
		fprintf(stderr, "config_parse\n");
		return 1;
	}

	if (interface_open() < 0) {
		fprintf(stderr, "interface_open\n");
		return -1;
	}

	// print info

	return 0;
}

static int command_write()
{
	if (config_parse() < 0) {
		fprintf(stderr, "config_parse\n");
		return 1;
	}

	if (address == 0xFFFFFFFF) {
		fprintf(stderr, "invalid address\n");
		return -1;
	}

	if (file == NULL) {
		if (file_path == NULL) {
			fprintf(stderr, "missing input file\n");
			return -1;
		}
		file = file_open(file_path, 0, -1);
		if (file == NULL) {
			fprintf(stderr, "file_open: %s\n", file_path);
			return -1;
		}
	}

	if (interface_open() < 0) {
		fprintf(stderr, "interface_open\n");
		return -1;
	}

	if (type == TYPE_DRAM) {
		if (nuc_write_dram(nuc, run, address, file->buf, file->count,
						"dram write", progress) < 0) {
			printf("\n");
			fprintf(stderr, "nuc_write_dram\n");
			return -1;
		}
	} else if(type == TYPE_SPINOR) {
		if (nuc_write_spi_nor(nuc, address, file->buf, file->count,
						"spinor write", progress) < 0) {
			printf("\n");
			fprintf(stderr, "nuc_write_spi_nor\n");
			return -1;
		}
		printf("\n");
		if (nuc_write_spi_nor_sync(nuc, "spinor sync", progress) < 0) {
			printf("\n");
			fprintf(stderr, "nuc_write_spi_nor_sync\n");
			return -1;
		}
	} else if(type == TYPE_SPINAND) {
		if (nuc_write_spi_nand(nuc, address, file->buf, file->count,
						"spinand write", progress) < 0) {
			printf("\n");
			fprintf(stderr, "nuc_write_spi_nand\n");
			return -1;
		}
		printf("\n");
		if (nuc_write_spi_nand_sync(nuc, "spinand sync", progress) < 0) {
			printf("\n");
			fprintf(stderr, "nuc_write_spi_nand_sync\n");
			return -1;
		}
	} else {
		fprintf(stderr, "invalid type\n");
		return -1;
	}
	printf("\n");
	return 0;
}

static int command_boot()
{
	uint32_t *loader = (uint32_t *)&loader_start;

	if (type != TYPE_SPINAND) {
		fprintf(stderr, "invalid type\n");
		return 1;
	}

	if (config_parse() < 0) {
		fprintf(stderr, "config_parse\n");
		return 1;
	}

	if (interface_open() < 0) {
		fprintf(stderr, "interface_open\n");
		return -1;
	}

	loader[1] = 0x7F6000D4;
	if (type == TYPE_SPINAND) {
		loader[1] |= ((uint32_t)0x03 << 0);
		loader[1] |= ((uint32_t)0x01 << 8);
	}

	if (pwron_set) {
		loader[2] = pwron;
	} else {
		loader[2] = loader[1];
	}

	loader[3] = 0x00;

	if (nuc_write_dram(nuc, 1, 0xF00000, &loader_start,
				&loader_end - &loader_start, NULL, NULL) < 0) {
		printf("\n");
		fprintf(stderr, "nuc_write_dram\n");
		return -1;
	}
	printf("boot\n");

	return 0;
}

static int command_reboot()
{
	uint32_t *loader = (uint32_t *)&loader_start;

	if (config_parse() < 0) {
		fprintf(stderr, "config_parse\n");
		return 1;
	}

	if (interface_open() < 0) {
		fprintf(stderr, "interface_open\n");
		return -1;
	}

	loader[3] = 0x01;

	if (nuc_write_dram(nuc, 1, 0xF00000, &loader_start,
				&loader_end - &loader_start, NULL, NULL) < 0) {
		printf("\n");
		fprintf(stderr, "nuc_write_dram\n");
		return -1;
	}
	printf("reboot\n");

	return 0;
}

static int command_nandfix()
{
	uint32_t *loader = (uint32_t *)&loader_start;

	if (config_parse() < 0) {
		fprintf(stderr, "config_parse\n");
		return 1;
	}

	if (interface_open() < 0) {
		fprintf(stderr, "interface_open\n");
		return -1;
	}

	loader[3] = 0x02;

	if (nuc_write_dram(nuc, 1, 0xF00000, &loader_start,
				&loader_end - &loader_start, NULL, NULL) < 0) {
		printf("\n");
		fprintf(stderr, "nuc_write_dram\n");
		return -1;
	}
	printf("nandfix\n");

	return 0;
}

