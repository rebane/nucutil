#include <stdio.h>
#include <stdint.h>
#include "nuc980.h"
#include "mask.h"
#include "incbin.h"
#include "cache.h"
#include "serial.h"
#include "qspi.h"
#include "spi_nand.h"

INCLUDE_BINARY_FILE(trampoline_start, trampoline_end, "trampoline.bin", ".data", "");
static void (*trampoline_code)() = (void *)0x3C000000;

extern uint32_t pwron1;
extern uint32_t pwron2;
extern uint32_t boot_cmd;
extern uint32_t boot_arg;

static char *boot_mode_list[] = { "USB", "SD/eMMC", "NAND", "SPI" };
static char *sd_controller_list[] = { "SD1", "SD1", "SD1", "SD0" };
static char *spi_mode_list[] = { "NAND, 1-bit", "NAND, 4-bit", "NOR, 4-bit", "NOR, 1-bit" };

static void load_spi_nand();
static void reboot();
static void nand_fix();

int main(void){
	int boot_mode, sd_controller, spi_mode;

	serial_init();
	printf("\rfinish SDRAM download\n\n");

	cache_disable();
	cache_flush_id();
	cache_enable_write_back();

	printf("Loader for NUC980\n");
	printf("2020, Rebane, rebane@alkohol.ee\n\n");

	if (boot_cmd == 0x00) { // loader
		printf("CMD: loader\n");
	} else if (boot_cmd == 0x01) { // reboot
		printf("CMD: reboot\n");
		reboot();
	} else if (boot_cmd == 0x02) { // nand fix
		printf("CMD: nand fix\n");
		nand_fix();
	}

	boot_mode = mask32_get(pwron1, SYS_PWRON_BTSSEL);
	printf("Boot mode: %s\n", boot_mode_list[boot_mode]);
	if(boot_mode == 1){
		sd_controller = mask32_get(pwron1, SYS_PWRON_MISCCFG);
		printf("SD controller: %s\n", sd_controller_list[sd_controller]);
		if(sd_controller == 3){
			printf("SD0 boot not supported yet\n");
		}else{
			printf("SD1 boot not supported yet\n");
		}
	}else if(boot_mode == 2){
		printf("NAND boot not supported yet\n");
	}else if(boot_mode == 3){
		spi_mode = mask32_get(pwron1, SYS_PWRON_MISCCFG);
		printf("SPI mode: %s\n", spi_mode_list[spi_mode]);
		if((spi_mode == 0) || (spi_mode == 1)){
			load_spi_nand();
		}else{
			printf("SPI NOR boot not supported yet\n");
		}
	}

	memcpy(trampoline_code, &trampoline_start, &trampoline_end - &trampoline_start);
	printf("Booting next stage bootloader...\n");

	trampoline_code();

	while(1);
}

static void load_spi_nand(){
	spi_nand_t nand;
	spi_nand_partition_t nandpart;
	uint32_t load_addr = LOAD_ADDR;
	uint32_t count, offset = 416;

	qspi_init();
	spi_nand_init(&nand, NULL, qspi_transfer_half, 0);
	spi_nand_part_init(&nandpart, &nand, 0, 1024 * 1024);

	spi_nand_part_read(&nandpart, (void *)load_addr, 0, 64);
	count = *(uint32_t *)(load_addr + 8);

	if (*(uint32_t *)(load_addr + 0x24) == 0x26)
		offset = 0x160;
	else if (*(uint32_t *)(load_addr + 0x24) == 0x2E)
		offset = 0x1A0;

	spi_nand_part_read(&nandpart, (void *)(load_addr + offset), offset, count);

	printf("\n");
	if ((*(int32_t *)(load_addr + offset + 12) == 0xAA5533EE) && (*(int32_t *)(load_addr + offset + 16) == 0x775599FF)) {
		*(int32_t *)(load_addr + offset + 4) = pwron2;
		*(int32_t *)(load_addr + offset + 8) = 1;
		printf("Boot marker set: 0x%08X\n", (unsigned int)(*(int32_t *)(load_addr + offset + 4)));
	} else if ((*(int32_t *)(load_addr + offset + 12) == 0xE4C31001) && (*(int32_t *)(load_addr + offset + 16) == 0xEAFFFFF8)) {
		*(int32_t *)(load_addr + offset + 4) = pwron2;
		*(int32_t *)(load_addr + offset + 8) = 1;
		printf("Boot marker set: 0x%08X\n", (unsigned int)(*(int32_t *)(load_addr + offset + 4)));
	}

	printf("Boot code: 0x%08x, Exec addr: 0x%08x, Count: 0x%08x\n", (unsigned int)(*(uint32_t *)(load_addr + 0)), (unsigned int)(*(uint32_t *)(load_addr + 4)), (unsigned int)count);
}

static void reboot(){
	static volatile uint32_t i;
	for (i = 0; i < 300000; i++)__asm__("nop");
	while(SYS->REGWPCTL != 1){
		SYS->REGWPCTL = 0x59;
		SYS->REGWPCTL = 0x16;
		SYS->REGWPCTL = 0x88;
	}
	SYS->AHBIPRST = 1;
	while(1);
}

static void nand_fix(){
	spi_nand_t nand;

	qspi_init();
	spi_nand_init(&nand, NULL, qspi_transfer_half, 0);

	reboot();
}

ssize_t write_stdout(const void *buf, size_t count){
	size_t i;
	for(i = 0; i < count; i++){
		if((((uint8_t *)buf)[i]) == '\n')serial_write("\r", 1);
		serial_write(&(((uint8_t *)buf)[i]), 1);
	}
	return(count);
}

