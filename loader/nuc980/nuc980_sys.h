#ifndef _NUC980_SYS_H_
#define _NUC980_SYS_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t PDID;
	volatile uint32_t PWRON;
	uint32_t RESERVED1[6];
	volatile uint32_t LVRDCR;
	uint32_t RESERVED2[3];
	volatile uint32_t MISCFCR;
	uint32_t RESERVED3[3];
	volatile uint32_t MISCIER;
	volatile uint32_t MISCISR;
	uint32_t RESERVED4[2];
	volatile uint32_t WKUPSER0;
	volatile uint32_t WKUPSER1;
	volatile uint32_t WKUPSSR0;
	volatile uint32_t WKUPSSR1;
	volatile uint32_t AHBIPRST;
	volatile uint32_t APBIPRST0;
	volatile uint32_t APBIPRST1;
	volatile uint32_t RSTSTS;
	volatile uint32_t GPA_MFPL;
	volatile uint32_t GPA_MFPH;
	volatile uint32_t GPB_MFPL;
	volatile uint32_t GPB_MFPH;
	volatile uint32_t GPC_MFPL;
	volatile uint32_t GPC_MFPH;
	volatile uint32_t GPD_MFPL;
	volatile uint32_t GPD_MFPH;
	volatile uint32_t GPE_MFPL;
	volatile uint32_t GPE_MFPH;
	volatile uint32_t GPF_MFPL;
	volatile uint32_t GPF_MFPH;
	volatile uint32_t GPG_MFPL;
	volatile uint32_t GPG_MFPH;
	uint32_t RESERVED5[18];
	volatile uint32_t DDR_DSCTL;
	volatile uint32_t GPBL_DSCTL;
	uint32_t RESERVED6[2];
	volatile uint32_t PORDISCR;
	uint32_t RESERVED7[62];
	volatile uint32_t REGWPCTL;
}SYS_TypeDef;

#define SYS                    ((SYS_TypeDef *)0xB0000000)

// SYS_PWRON
#define SYS_PWRON_BTSSEL       (((uint32_t)0x03) << 0)
#define SYS_PWRON_QSPI0CKSEL   (((uint32_t)0x01) << 2)
#define SYS_PWRON_WDTON        (((uint32_t)0x01) << 3)
#define SYS_PWRON_JTAGSEL      (((uint32_t)0x01) << 4)
#define SYS_PWRON_URDBGON      (((uint32_t)0x01) << 5)
#define SYS_PWRON_NPAGESEL     (((uint32_t)0x03) << 6)
#define SYS_PWRON_MISCCFG      (((uint32_t)0x03) << 8)
#define SYS_PWRON_USBID        (((uint32_t)0x01) << 16)
#define SYS_PWRON_TICMOD       (((uint32_t)0x01) << 17)
#define SYS_PWRON_DRAMSIZE     (((uint32_t)0x07) << 20)
#define SYS_PWRON_USERID       (((uint32_t)0x1F) << 24)

// SYS_GPA_MFPL
#define SYS_GPA_MFPL_MFP_GPA0  (((uint32_t)0x0F) << 0)
#define SYS_GPA_MFPL_MFP_GPA1  (((uint32_t)0x0F) << 4)
#define SYS_GPA_MFPL_MFP_GPA2  (((uint32_t)0x0F) << 8)
#define SYS_GPA_MFPL_MFP_GPA3  (((uint32_t)0x0F) << 12)
#define SYS_GPA_MFPL_MFP_GPA4  (((uint32_t)0x0F) << 16)
#define SYS_GPA_MFPL_MFP_GPA5  (((uint32_t)0x0F) << 20)
#define SYS_GPA_MFPL_MFP_GPA6  (((uint32_t)0x0F) << 24)
#define SYS_GPA_MFPL_MFP_GPA7  (((uint32_t)0x0F) << 28)

// SYS_GPA_MFPH
#define SYS_GPA_MFPH_MFP_GPA8  (((uint32_t)0x0F) << 0)
#define SYS_GPA_MFPH_MFP_GPA9  (((uint32_t)0x0F) << 4)
#define SYS_GPA_MFPH_MFP_GPA10 (((uint32_t)0x0F) << 8)
#define SYS_GPA_MFPH_MFP_GPA11 (((uint32_t)0x0F) << 12)
#define SYS_GPA_MFPH_MFP_GPA12 (((uint32_t)0x0F) << 16)
#define SYS_GPA_MFPH_MFP_GPA13 (((uint32_t)0x0F) << 20)
#define SYS_GPA_MFPH_MFP_GPA14 (((uint32_t)0x0F) << 24)
#define SYS_GPA_MFPH_MFP_GPA15 (((uint32_t)0x0F) << 28)

// SYS_GPB_MFPL
#define SYS_GPB_MFPL_MFP_GPB0  (((uint32_t)0x0F) << 0)
#define SYS_GPB_MFPL_MFP_GPB1  (((uint32_t)0x0F) << 4)
#define SYS_GPB_MFPL_MFP_GPB2  (((uint32_t)0x0F) << 8)
#define SYS_GPB_MFPL_MFP_GPB3  (((uint32_t)0x0F) << 12)
#define SYS_GPB_MFPL_MFP_GPB4  (((uint32_t)0x0F) << 16)
#define SYS_GPB_MFPL_MFP_GPB5  (((uint32_t)0x0F) << 20)
#define SYS_GPB_MFPL_MFP_GPB6  (((uint32_t)0x0F) << 24)
#define SYS_GPB_MFPL_MFP_GPB7  (((uint32_t)0x0F) << 28)

// SYS_GPB_MFPH
#define SYS_GPB_MFPH_MFP_GPB8  (((uint32_t)0x0F) << 0)
#define SYS_GPB_MFPH_MFP_GPB9  (((uint32_t)0x0F) << 4)
#define SYS_GPB_MFPH_MFP_GPB10 (((uint32_t)0x0F) << 8)
#define SYS_GPB_MFPH_MFP_GPB11 (((uint32_t)0x0F) << 12)
#define SYS_GPB_MFPH_MFP_GPB12 (((uint32_t)0x0F) << 16)
#define SYS_GPB_MFPH_MFP_GPB13 (((uint32_t)0x0F) << 20)
#define SYS_GPB_MFPH_MFP_GPB14 (((uint32_t)0x0F) << 24)
#define SYS_GPB_MFPH_MFP_GPB15 (((uint32_t)0x0F) << 28)

// SYS_GPC_MFPL
#define SYS_GPC_MFPL_MFP_GPC0  (((uint32_t)0x0F) << 0)
#define SYS_GPC_MFPL_MFP_GPC1  (((uint32_t)0x0F) << 4)
#define SYS_GPC_MFPL_MFP_GPC2  (((uint32_t)0x0F) << 8)
#define SYS_GPC_MFPL_MFP_GPC3  (((uint32_t)0x0F) << 12)
#define SYS_GPC_MFPL_MFP_GPC4  (((uint32_t)0x0F) << 16)
#define SYS_GPC_MFPL_MFP_GPC5  (((uint32_t)0x0F) << 20)
#define SYS_GPC_MFPL_MFP_GPC6  (((uint32_t)0x0F) << 24)
#define SYS_GPC_MFPL_MFP_GPC7  (((uint32_t)0x0F) << 28)

// SYS_GPC_MFPH
#define SYS_GPC_MFPH_MFP_GPC8  (((uint32_t)0x0F) << 0)
#define SYS_GPC_MFPH_MFP_GPC9  (((uint32_t)0x0F) << 4)
#define SYS_GPC_MFPH_MFP_GPC10 (((uint32_t)0x0F) << 8)
#define SYS_GPC_MFPH_MFP_GPC11 (((uint32_t)0x0F) << 12)
#define SYS_GPC_MFPH_MFP_GPC12 (((uint32_t)0x0F) << 16)
#define SYS_GPC_MFPH_MFP_GPC13 (((uint32_t)0x0F) << 20)
#define SYS_GPC_MFPH_MFP_GPC14 (((uint32_t)0x0F) << 24)
#define SYS_GPC_MFPH_MFP_GPC15 (((uint32_t)0x0F) << 28)

// SYS_GPD_MFPL
#define SYS_GPD_MFPL_MFP_GPD0  (((uint32_t)0x0F) << 0)
#define SYS_GPD_MFPL_MFP_GPD1  (((uint32_t)0x0F) << 4)
#define SYS_GPD_MFPL_MFP_GPD2  (((uint32_t)0x0F) << 8)
#define SYS_GPD_MFPL_MFP_GPD3  (((uint32_t)0x0F) << 12)
#define SYS_GPD_MFPL_MFP_GPD4  (((uint32_t)0x0F) << 16)
#define SYS_GPD_MFPL_MFP_GPD5  (((uint32_t)0x0F) << 20)
#define SYS_GPD_MFPL_MFP_GPD6  (((uint32_t)0x0F) << 24)
#define SYS_GPD_MFPL_MFP_GPD7  (((uint32_t)0x0F) << 28)

// SYS_GPD_MFPH
#define SYS_GPD_MFPH_MFP_GPD8  (((uint32_t)0x0F) << 0)
#define SYS_GPD_MFPH_MFP_GPD9  (((uint32_t)0x0F) << 4)
#define SYS_GPD_MFPH_MFP_GPD10 (((uint32_t)0x0F) << 8)
#define SYS_GPD_MFPH_MFP_GPD11 (((uint32_t)0x0F) << 12)
#define SYS_GPD_MFPH_MFP_GPD12 (((uint32_t)0x0F) << 16)
#define SYS_GPD_MFPH_MFP_GPD13 (((uint32_t)0x0F) << 20)
#define SYS_GPD_MFPH_MFP_GPD14 (((uint32_t)0x0F) << 24)
#define SYS_GPD_MFPH_MFP_GPD15 (((uint32_t)0x0F) << 28)

// SYS_GPE_MFPL
#define SYS_GPE_MFPL_MFP_GPE0  (((uint32_t)0x0F) << 0)
#define SYS_GPE_MFPL_MFP_GPE1  (((uint32_t)0x0F) << 4)
#define SYS_GPE_MFPL_MFP_GPE2  (((uint32_t)0x0F) << 8)
#define SYS_GPE_MFPL_MFP_GPE3  (((uint32_t)0x0F) << 12)
#define SYS_GPE_MFPL_MFP_GPE4  (((uint32_t)0x0F) << 16)
#define SYS_GPE_MFPL_MFP_GPE5  (((uint32_t)0x0F) << 20)
#define SYS_GPE_MFPL_MFP_GPE6  (((uint32_t)0x0F) << 24)
#define SYS_GPE_MFPL_MFP_GPE7  (((uint32_t)0x0F) << 28)

// SYS_GPE_MFPH
#define SYS_GPE_MFPH_MFP_GPE8  (((uint32_t)0x0F) << 0)
#define SYS_GPE_MFPH_MFP_GPE9  (((uint32_t)0x0F) << 4)
#define SYS_GPE_MFPH_MFP_GPE10 (((uint32_t)0x0F) << 8)
#define SYS_GPE_MFPH_MFP_GPE11 (((uint32_t)0x0F) << 12)
#define SYS_GPE_MFPH_MFP_GPE12 (((uint32_t)0x0F) << 16)
#define SYS_GPE_MFPH_MFP_GPE13 (((uint32_t)0x0F) << 20)
#define SYS_GPE_MFPH_MFP_GPE14 (((uint32_t)0x0F) << 24)
#define SYS_GPE_MFPH_MFP_GPE15 (((uint32_t)0x0F) << 28)

// SYS_GPF_MFPL
#define SYS_GPF_MFPL_MFP_GPF0  (((uint32_t)0x0F) << 0)
#define SYS_GPF_MFPL_MFP_GPF1  (((uint32_t)0x0F) << 4)
#define SYS_GPF_MFPL_MFP_GPF2  (((uint32_t)0x0F) << 8)
#define SYS_GPF_MFPL_MFP_GPF3  (((uint32_t)0x0F) << 12)
#define SYS_GPF_MFPL_MFP_GPF4  (((uint32_t)0x0F) << 16)
#define SYS_GPF_MFPL_MFP_GPF5  (((uint32_t)0x0F) << 20)
#define SYS_GPF_MFPL_MFP_GPF6  (((uint32_t)0x0F) << 24)
#define SYS_GPF_MFPL_MFP_GPF7  (((uint32_t)0x0F) << 28)

// SYS_GPF_MFPH
#define SYS_GPF_MFPH_MFP_GPF8  (((uint32_t)0x0F) << 0)
#define SYS_GPF_MFPH_MFP_GPF9  (((uint32_t)0x0F) << 4)
#define SYS_GPF_MFPH_MFP_GPF10 (((uint32_t)0x0F) << 8)
#define SYS_GPF_MFPH_MFP_GPF11 (((uint32_t)0x0F) << 12)
#define SYS_GPF_MFPH_MFP_GPF12 (((uint32_t)0x0F) << 16)
#define SYS_GPF_MFPH_MFP_GPF13 (((uint32_t)0x0F) << 20)
#define SYS_GPF_MFPH_MFP_GPF14 (((uint32_t)0x0F) << 24)
#define SYS_GPF_MFPH_MFP_GPF15 (((uint32_t)0x0F) << 28)

// SYS_GPG_MFPL
#define SYS_GPG_MFPL_MFP_GPG0  (((uint32_t)0x0F) << 0)
#define SYS_GPG_MFPL_MFP_GPG1  (((uint32_t)0x0F) << 4)
#define SYS_GPG_MFPL_MFP_GPG2  (((uint32_t)0x0F) << 8)
#define SYS_GPG_MFPL_MFP_GPG3  (((uint32_t)0x0F) << 12)
#define SYS_GPG_MFPL_MFP_GPG4  (((uint32_t)0x0F) << 16)
#define SYS_GPG_MFPL_MFP_GPG5  (((uint32_t)0x0F) << 20)
#define SYS_GPG_MFPL_MFP_GPG6  (((uint32_t)0x0F) << 24)
#define SYS_GPG_MFPL_MFP_GPG7  (((uint32_t)0x0F) << 28)

// SYS_GPG_MFPH
#define SYS_GPG_MFPH_MFP_GPG8  (((uint32_t)0x0F) << 0)
#define SYS_GPG_MFPH_MFP_GPG9  (((uint32_t)0x0F) << 4)
#define SYS_GPG_MFPH_MFP_GPG10 (((uint32_t)0x0F) << 8)
#define SYS_GPG_MFPH_MFP_GPG11 (((uint32_t)0x0F) << 12)
#define SYS_GPG_MFPH_MFP_GPG12 (((uint32_t)0x0F) << 16)
#define SYS_GPG_MFPH_MFP_GPG13 (((uint32_t)0x0F) << 20)
#define SYS_GPG_MFPH_MFP_GPG14 (((uint32_t)0x0F) << 24)
#define SYS_GPG_MFPH_MFP_GPG15 (((uint32_t)0x0F) << 28)

#endif

