#include "qspi.h"
#include "nuc980.h"
#include "mask.h"

void qspi_init(){
	CLK->HCLKEN |= CLK_HCLKEN_GPIO;
	CLK->PCLKEN1 |= CLK_PCLKEN1_QSPI0CKEN;
	
	mask32_set(SYS->GPD_MFPL, SYS_GPD_MFPL_MFP_GPD2, 1);
	mask32_set(SYS->GPD_MFPL, SYS_GPD_MFPL_MFP_GPD3, 1);
	mask32_set(SYS->GPD_MFPL, SYS_GPD_MFPL_MFP_GPD4, 1);
	mask32_set(SYS->GPD_MFPL, SYS_GPD_MFPL_MFP_GPD5, 1);
	mask32_set(SYS->GPD_MFPL, SYS_GPD_MFPL_MFP_GPD6, 0);
	mask32_set(SYS->GPD_MFPL, SYS_GPD_MFPL_MFP_GPD7, 0);
	
	mask32_set(GPIOD->MODE, GPIO_MODE_MODE6, 1);
	mask32_set(GPIOD->MODE, GPIO_MODE_MODE7, 1);
	GPIOD->DOUT |= GPIO_DOUT_DOUT6 | GPIO_DOUT_DOUT7;

	// Configure QSPI0 as a master, MSB first, 8-bit transaction, QSPI Mode-0 timing, clock is 50MHz
	QSPI->SSCTL = 0;
	QSPI->CTL = mask32(SPI_CTL_DWIDTH, 8) | SPI_CTL_TXNEG | SPI_CTL_EN;
	QSPI->CLKDIV = ((150000000LU / 50000000LU) - 1LU);
}

void qspi_cs(int cs, int active){
	if((cs == 0) && active){
		QSPI->SSCTL |= SPI_SSCTL_SS;
	}else{
		QSPI->SSCTL &= ~SPI_SSCTL_SS;
	}
}

void qspi_transfer_half(void *user, int cs, const void *out, int out_len, void *in, int in_len){
	int i, r;
	while(!(QSPI->STATUS & SPI_STATUS_RXEMPTY))(void)QSPI->RX;
	r = 0;
	qspi_cs(cs, 1);
	// SEND
	for(i = 0; i < out_len; i++){
		while(QSPI->STATUS & SPI_STATUS_TXFULL);
		QSPI->TX = ((uint8_t *)out)[i];
		if(!(QSPI->STATUS & SPI_STATUS_RXEMPTY)){
			(void)QSPI->RX;
			r++;
		}
	}
	while(r < out_len){
		if(!(QSPI->STATUS & SPI_STATUS_RXEMPTY)){
			(void)QSPI->RX;
			r++;
		}
	}
	// RECV
	r = 0;
	for(i = 0; i < in_len; i++){
		while(QSPI->STATUS & SPI_STATUS_TXFULL);
		QSPI->TX = 0;
		if(!(QSPI->STATUS & SPI_STATUS_RXEMPTY)){
			((uint8_t *)in)[r++] = QSPI->RX;
		}
	}
	while(r < in_len){
		if(!(QSPI->STATUS & SPI_STATUS_RXEMPTY)){
			((uint8_t *)in)[r++] = QSPI->RX;
		}
	}
	qspi_cs(cs, 0);
}

