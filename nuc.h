#ifndef _NUC_H_
#define _NUC_H_

#include <stdint.h>
#include "usb.h"

#define NUC_VENDOR_ID    0x0416
#define NUC_PRODUCT_ID   0x5963

#define NUC_TYPE_SDRAM   0
#define NUC_TYPE_SPINOR  7
#define NUC_TYPE_SPINAND 9
#define NUC_TYPE_INFO    10

typedef struct nuc_struct nuc_t;

struct nuc_info_struct{
	struct{
		int ok;
		uint32_t pages_per_block;
		uint32_t page_size;
		uint32_t sectors_per_block;
		uint32_t blocks_per_flash;
		uint32_t bad_block_count;
		uint32_t spare_size;
		uint32_t is_user_config;
	}nand;
	struct{
		int ok;
		uint32_t id;
		uint8_t quad_read_cmd;
		uint8_t read_status_cmd;
		uint8_t write_status_cmd;
		uint8_t status;
		uint32_t is_user_config;
	}spi_nor;
	struct{
		int ok;
		uint32_t id;
		uint32_t page_size;
		uint32_t spare_area;
		uint8_t quad_read_cmd;
		uint8_t read_status_cmd;
		uint8_t write_status_cmd;
		uint8_t status;
		uint32_t is_user_config;
	}spi_nand;
	struct{
		int ok;
		uint32_t block;
		uint32_t reserved;
	}emmc;
};

typedef struct nuc_info_struct nuc_info_t;

nuc_t *nuc_open(usb_path_t *path);
int nuc_ddr(nuc_t *n, const void *buf, int count);
int nuc_fw(nuc_t *n, uint32_t address, const void *buf, int count);
int nuc_info(nuc_t *n, nuc_info_t *info);

int nuc_write_dram(nuc_t *n, int run, uint32_t address, const void *buf, int count, void *user, void(*progress)(void *user, int written, int count));

int nuc_write_spi_nand(nuc_t *n, uint32_t address, const void *buf, int count, void *user, void(*progress)(void *user, int written, int count));
int nuc_write_spi_nand_sync(nuc_t *n, void *user, void(*progress)(void *user, int written, int count));

int nuc_write_spi_nor(nuc_t *n, uint32_t address, const void *buf, int count, void *user, void(*progress)(void *user, int written, int count));
int nuc_write_spi_nor_sync(nuc_t *n, void *user, void(*progress)(void *user, int written, int count));

void nuc_close(nuc_t *n);

#endif

