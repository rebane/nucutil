#include "serial.h"
#include <stdint.h>
#include "nuc980.h"
#include "mask.h"

static uint32_t serial_baud_div(uint32_t clock, uint32_t baud){
	return((((clock) + ((baud) / 2LU)) / (baud)) - 2LU);
}

void serial_init(){
	CLK->PCLKEN0 |= CLK_PCLKEN0_UART0CKEN;

	mask32_set(SYS->GPF_MFPH, SYS_GPF_MFPH_MFP_GPF11, 1);
	mask32_set(SYS->GPF_MFPH, SYS_GPF_MFPH_MFP_GPF12, 1);

	UART0->FCR = UART_FCR_RXRST | UART_FCR_TXRST;
	UART0->LCR |= (UART_LCR_WLS | UART_LCR_NSB);
	UART0->BAUD = UART_BAUD_BAUDM0 | UART_BAUD_BAUDM1 | serial_baud_div(12000000, 115200);
}

void serial_putc(unsigned char c){
	while(UART0->FSR & UART_FSR_TXFULL);
	UART0->THR = c;
}

int16_t serial_write(const void *buf, int16_t count){
	uint16_t i;
	if(count < 1)return(count);
	for(i = 0; i < count; i++){
		serial_putc(((uint8_t *)buf)[i]);
	}
	return(count);
}

