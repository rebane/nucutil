#ifndef _SERIAL_H_
#define _SERIAL_H_

#include <stdint.h>

void serial_init();
void serial_putc(unsigned char c);
int16_t serial_write(const void *buf, int16_t count);

#endif

