#ifndef _NUC980_CLK_H_
#define _NUC980_CLK_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t PMCON;
	uint32_t RESERVED1[3];
	volatile uint32_t HCLKEN;
	uint32_t RESERVED2[1];
	volatile uint32_t PCLKEN0;
	volatile uint32_t PCLKEN1;
	union{
		struct{
			volatile uint32_t DIVCTL0;
			volatile uint32_t DIVCTL1;
			volatile uint32_t DIVCTL2;
			volatile uint32_t DIVCTL3;
			volatile uint32_t DIVCTL4;
			volatile uint32_t DIVCTL5;
			volatile uint32_t DIVCTL6;
			volatile uint32_t DIVCTL7;
			volatile uint32_t DIVCTL8;
			volatile uint32_t DIVCTL9;
		};
		volatile uint32_t DIVCTL[10];
	};
	uint32_t RESERVED3[6];
	volatile uint32_t APLLCON;
	volatile uint32_t UPLLCON;
	uint32_t RESERVED4[6];
	volatile uint32_t PLLSTBCNTR;
}CLK_TypeDef;

#define CLK                  ((CLK_TypeDef *)0xB0000200)

// CLK_HCLKEN
#define CLK_HCLKEN_CPU       (((uint32_t)0x01) << 0)
#define CLK_HCLKEN_HCLK      (((uint32_t)0x01) << 1)
#define CLK_HCLKEN_HCLK1     (((uint32_t)0x01) << 2)
#define CLK_HCLKEN_HCLK3     (((uint32_t)0x01) << 3)
#define CLK_HCLKEN_HCLK4     (((uint32_t)0x01) << 4)
#define CLK_HCLKEN_PCLK0     (((uint32_t)0x01) << 5)
#define CLK_HCLKEN_PCLK1     (((uint32_t)0x01) << 6)
#define CLK_HCLKEN_TIC       (((uint32_t)0x01) << 7)
#define CLK_HCLKEN_SRAM      (((uint32_t)0x01) << 8)
#define CLK_HCLKEN_EBI       (((uint32_t)0x01) << 9)
#define CLK_HCLKEN_SDIC      (((uint32_t)0x01) << 10)
#define CLK_HCLKEN_GPIO      (((uint32_t)0x01) << 11)
#define CLK_HCLKEN_PDMA0     (((uint32_t)0x01) << 12)
#define CLK_HCLKEN_PDMA1     (((uint32_t)0x01) << 13)
#define CLK_HCLKEN_PCLK2     (((uint32_t)0x01) << 14)
#define CLK_HCLKEN_CKO       (((uint32_t)0x01) << 15)
#define CLK_HCLKEN_EMAC0     (((uint32_t)0x01) << 16)
#define CLK_HCLKEN_EMAC1     (((uint32_t)0x01) << 17)
#define CLK_HCLKEN_USBH      (((uint32_t)0x01) << 18)
#define CLK_HCLKEN_USBD      (((uint32_t)0x01) << 19)
#define CLK_HCLKEN_FMI       (((uint32_t)0x01) << 20)
#define CLK_HCLKEN_NAND      (((uint32_t)0x01) << 21)
#define CLK_HCLKEN_SD0       (((uint32_t)0x01) << 22)
#define CLK_HCLKEN_CRYPTO    (((uint32_t)0x01) << 23)
#define CLK_HCLKEN_I2S       (((uint32_t)0x01) << 24)
#define CLK_HCLKEN_VACP0     (((uint32_t)0x01) << 26)
#define CLK_HCLKEN_SENSOR    (((uint32_t)0x01) << 27)
#define CLK_HCLKEN_SD1       (((uint32_t)0x01) << 30)
#define CLK_HCLKEN_VCAP1     (((uint32_t)0x01) << 31)

// CLK_PCLKEN0
#define CLK_PCLKEN0_WDTCKEN   (((uint32_t)0x01) << 0)
#define CLK_PCLKEN0_WWDTCKEN  (((uint32_t)0x01) << 1)
#define CLK_PCLKEN0_RTCCKEN   (((uint32_t)0x01) << 2)
#define CLK_PCLKEN0_TMR0CKEN  (((uint32_t)0x01) << 8)
#define CLK_PCLKEN0_TMR1CKEN  (((uint32_t)0x01) << 9)
#define CLK_PCLKEN0_TMR2CKEN  (((uint32_t)0x01) << 10)
#define CLK_PCLKEN0_TMR3CKEN  (((uint32_t)0x01) << 11)
#define CLK_PCLKEN0_TMR4CKEN  (((uint32_t)0x01) << 12)
#define CLK_PCLKEN0_TMR5CKEN  (((uint32_t)0x01) << 13)
#define CLK_PCLKEN0_UART0CKEN (((uint32_t)0x01) << 16)
#define CLK_PCLKEN0_UART1CKEN (((uint32_t)0x01) << 17)
#define CLK_PCLKEN0_UART2CKEN (((uint32_t)0x01) << 18)
#define CLK_PCLKEN0_UART3CKEN (((uint32_t)0x01) << 19)
#define CLK_PCLKEN0_UART4CKEN (((uint32_t)0x01) << 20)
#define CLK_PCLKEN0_UART5CKEN (((uint32_t)0x01) << 21)
#define CLK_PCLKEN0_UART6CKEN (((uint32_t)0x01) << 22)
#define CLK_PCLKEN0_UART7CKEN (((uint32_t)0x01) << 23)
#define CLK_PCLKEN0_UART8CKEN (((uint32_t)0x01) << 24)
#define CLK_PCLKEN0_UART9CKEN (((uint32_t)0x01) << 25)

// CLK_PCLKEN1
#define CLK_PCLKEN1_I2C0CKEN  (((uint32_t)0x01) << 0)
#define CLK_PCLKEN1_I2C1CKEN  (((uint32_t)0x01) << 1)
#define CLK_PCLKEN1_I2C2CKEN  (((uint32_t)0x01) << 2)
#define CLK_PCLKEN1_I2C3CKEN  (((uint32_t)0x01) << 3)
#define CLK_PCLKEN1_QSPI0CKEN (((uint32_t)0x01) << 4)
#define CLK_PCLKEN1_SPI0CKEN  (((uint32_t)0x01) << 5)
#define CLK_PCLKEN1_SPI1CKEN  (((uint32_t)0x01) << 6)
#define CLK_PCLKEN1_CAN0CKEN  (((uint32_t)0x01) << 8)
#define CLK_PCLKEN1_CAN1CKEN  (((uint32_t)0x01) << 9)
#define CLK_PCLKEN1_CAN2CKEN  (((uint32_t)0x01) << 10)
#define CLK_PCLKEN1_CAN3CKEN  (((uint32_t)0x01) << 11)
#define CLK_PCLKEN1_SMC0CKEN  (((uint32_t)0x01) << 12)
#define CLK_PCLKEN1_SMC1CKEN  (((uint32_t)0x01) << 13)
#define CLK_PCLKEN1_ADCCKEN   (((uint32_t)0x01) << 24)
#define CLK_PCLKEN1_PWM0CKEN  (((uint32_t)0x01) << 26)
#define CLK_PCLKEN1_PWM1CKEN  (((uint32_t)0x01) << 27)

// CLK_DIVCTL3
#define CLK_DIVCTL3_SD0_S     (((uint32_t)0x03) << 3)
#define CLK_DIVCTL3_SD0_N     (((uint32_t)0xFF) << 8)

// CLK_DIVCTL9
#define CLK_DIVCTL9_SD1_S     (((uint32_t)0x03) << 3)
#define CLK_DIVCTL9_SD1_N     (((uint32_t)0xFF) << 8)

#endif

