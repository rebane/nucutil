#ifndef _NUC980_H_
#define _NUC980_H_

#include "nuc980_sys.h"
#include "nuc980_aic.h"
#include "nuc980_clk.h"
#include "nuc980_gpio.h"
#include "nuc980_uart.h"
#include "nuc980_spi.h"
#include "nuc980_sd.h"

#endif

