#ifndef _NUC980_SPI_H_
#define _NUC980_SPI_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t CTL;
	volatile uint32_t CLKDIV;
	volatile uint32_t SSCTL;
	volatile uint32_t PDMACTL;
	volatile uint32_t FIFOCTL;
	volatile uint32_t STATUS;
	uint32_t RESERVED1[2];
	volatile uint32_t TX;
	uint32_t RESERVED2[3];
	volatile uint32_t RX;
}SPI_TypeDef;

#define SPI0                 ((SPI_TypeDef *)0xB0061000)
#define SPI1                 ((SPI_TypeDef *)0xB0062000)
#define QSPI                 ((SPI_TypeDef *)0xB0060000)

// SPI_CTL
#define SPI_CTL_EN           (((uint32_t)0x01) << 0)
#define SPI_CTL_RXNEG        (((uint32_t)0x01) << 1)
#define SPI_CTL_TXNEG        (((uint32_t)0x01) << 2)
#define SPI_CTL_CLKPOL       (((uint32_t)0x01) << 3)
#define SPI_CTL_SUSPITV      (((uint32_t)0x0F) << 4)
#define SPI_CTL_DWIDTH       (((uint32_t)0x1F) << 8)
#define SPI_CTL_LSB          (((uint32_t)0x01) << 13)
#define SPI_CTL_HALFDPX      (((uint32_t)0x01) << 14)
#define SPI_CTL_RXONLY       (((uint32_t)0x01) << 15)
#define SPI_CTL_TWOBIT       (((uint32_t)0x01) << 16)
#define SPI_CTL_UNITIEN      (((uint32_t)0x01) << 17)
#define SPI_CTL_SLAVE        (((uint32_t)0x01) << 18)
#define SPI_CTL_REORDER      (((uint32_t)0x01) << 19)
#define SPI_CTL_DATDIR       (((uint32_t)0x01) << 20)
#define SPI_CTL_DUALIOEN     (((uint32_t)0x01) << 21)
#define SPI_CTL_QUADIOEN     (((uint32_t)0x01) << 22)

// SPI_SSCTL
#define SPI_SSCTL_SS         (((uint32_t)0x01) << 0)
#define SPI_SSCTL_SSACTPOL   (((uint32_t)0x01) << 2)
#define SPI_SSCTL_AUTOSS     (((uint32_t)0x01) << 3)
#define SPI_SSCTL_SLV3WIRE   (((uint32_t)0x01) << 4)
#define SPI_SSCTL_SLVTOIEN   (((uint32_t)0x01) << 5)
#define SPI_SSCTL_SLVTORST   (((uint32_t)0x01) << 6)
#define SPI_SSCTL_SLVBEIEN   (((uint32_t)0x01) << 8)
#define SPI_SSCTL_SLVURIEN   (((uint32_t)0x01) << 9)
#define SPI_SSCTL_SSACTIEN   (((uint32_t)0x01) << 12)
#define SPI_SSCTL_SSINAIEN   (((uint32_t)0x01) << 13)
#define SPI_SSCTL_SLVTOCNT   (((uint32_t)0xFFFF) << 16)

// SPI_PDMACTL
#define SPI_PDMACTL_TXPDMAEN (((uint32_t)0x01) << 0)
#define SPI_PDMACTL_RXPDMAEN (((uint32_t)0x01) << 1)
#define SPI_PDMACTL_PDMARST  (((uint32_t)0x01) << 2)

// SPI_FIFOCTL
#define SPI_FIFOCTL_RXRST    (((uint32_t)0x01) << 0)
#define SPI_FIFOCTL_TXRST    (((uint32_t)0x01) << 1)
#define SPI_FIFOCTL_RXTHIEN  (((uint32_t)0x01) << 2)
#define SPI_FIFOCTL_TXTHIEN  (((uint32_t)0x01) << 3)
#define SPI_FIFOCTL_RXTOIEN  (((uint32_t)0x01) << 4)
#define SPI_FIFOCTL_RXOVIEN  (((uint32_t)0x01) << 5)
#define SPI_FIFOCTL_TXUFPOL  (((uint32_t)0x01) << 6)
#define SPI_FIFOCTL_TXUFIEN  (((uint32_t)0x01) << 7)
#define SPI_FIFOCTL_RXFBCLR  (((uint32_t)0x01) << 8)
#define SPI_FIFOCTL_TXFBCLR  (((uint32_t)0x01) << 9)
#define SPI_FIFOCTL_RXTH     (((uint32_t)0x07) << 24)
#define SPI_FIFOCTL_TXTH     (((uint32_t)0x07) << 28)

// SPI_STATUS
#define SPI_STATUS_BUSY      (((uint32_t)0x01) << 0)
#define SPI_STATUS_UNITIF    (((uint32_t)0x01) << 1)
#define SPI_STATUS_SSACTIF   (((uint32_t)0x01) << 2)
#define SPI_STATUS_SSINAIF   (((uint32_t)0x01) << 3)
#define SPI_STATUS_SSLINE    (((uint32_t)0x01) << 4)
#define SPI_STATUS_SLVTOIF   (((uint32_t)0x01) << 5)
#define SPI_STATUS_SLVBEIF   (((uint32_t)0x01) << 6)
#define SPI_STATUS_SLVURIF   (((uint32_t)0x01) << 7)
#define SPI_STATUS_RXEMPTY   (((uint32_t)0x01) << 8)
#define SPI_STATUS_RXFULL    (((uint32_t)0x01) << 9)
#define SPI_STATUS_RXTHIF    (((uint32_t)0x01) << 10)
#define SPI_STATUS_RXOVIF    (((uint32_t)0x01) << 11)
#define SPI_STATUS_RXTOIF    (((uint32_t)0x01) << 12)
#define SPI_STATUS_ENSTS     (((uint32_t)0x01) << 15)
#define SPI_STATUS_TXEMPTY   (((uint32_t)0x01) << 16)
#define SPI_STATUS_TXFULL    (((uint32_t)0x01) << 17)
#define SPI_STATUS_TXTHIF    (((uint32_t)0x01) << 18)
#define SPI_STATUS_TXUFIF    (((uint32_t)0x01) << 19)
#define SPI_STATUS_TXRXRST   (((uint32_t)0x01) << 23)
#define SPI_STATUS_RXCNT     (((uint32_t)0x0F) << 24)
#define SPI_STATUS_TXCNT     (((uint32_t)0x0F) << 28)

#endif

