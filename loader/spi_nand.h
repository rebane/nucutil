#ifndef _SPI_NAND_H_
#define _SPI_NAND_H_

#include <stdint.h>

#define SPI_NAND_BLOCK_SIZE  (128 * 1024)
#define SPI_NAND_BLOCK_COUNT 1024
#define SPI_NAND_PAGE_SIZE   (2 * 1024)
#define SPI_NAND_OOB_SIZE    64
#define SPI_NAND_CACHE_SIZE  (SPI_NAND_PAGE_SIZE + SPI_NAND_OOB_SIZE)

typedef void (*spi_nand_transfer_half_t)(void *user, int cs, const void *out, int out_len, void *in, int in_len);

struct spi_nand_struct{
	spi_nand_transfer_half_t transfer_half;
	void *user;
	int cs;
	uint8_t cache[SPI_NAND_CACHE_SIZE];
	uint32_t cache_page;
	int cache_status;
	uint32_t id;
};

typedef struct spi_nand_struct spi_nand_t;

struct spi_nand_partition_struct{
	spi_nand_t *nand;
	uint32_t offset;
	uint32_t size;
	uint32_t block_count;
	uint8_t block_list[SPI_NAND_BLOCK_COUNT];
	uint32_t last_virtual_block;
	uint32_t last_physical_block;
};

typedef struct spi_nand_partition_struct spi_nand_partition_t;

void spi_nand_init(spi_nand_t *nand, void *user, spi_nand_transfer_half_t transfer_half, int cs);

void spi_nand_part_init(spi_nand_partition_t *snp, spi_nand_t *nand, uint32_t offset, uint32_t size);
int32_t spi_nand_part_copy(spi_nand_partition_t *dst_snp, spi_nand_partition_t *src_snp, int32_t count);
uint32_t spi_nand_part_phy_addr(spi_nand_partition_t *snp, uint32_t part_offset);
int32_t spi_nand_part_read(spi_nand_partition_t *snp, void *dest, uint32_t src_part_offset, int32_t count);

#endif

