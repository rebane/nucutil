#ifndef _NUC980_AIC_H_
#define _NUC980_AIC_H_

#include <stdint.h>

typedef struct{
	volatile uint32_t SRCCTL0;
	uint32_t RESERVED1[71];
	volatile uint32_t IRQNUM;
	uint32_t RESERVED2[3];
	volatile uint32_t INTEN0;
	volatile uint32_t INTEN1;
	volatile uint32_t INTDIS0;
	volatile uint32_t INTDIS1;
	uint32_t RESERVED3[4];
	volatile uint32_t EOIS;
}AIC_TypeDef;

#define AIC                  ((AIC_TypeDef *)0xB0042000)

#endif

